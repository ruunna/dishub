<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDatakendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('datakendaraans', function (Blueprint $table) { 
            $table->engine = 'InnoDB';
            $table->increments('idkendaraan');
            $table->string('nama_pembuat');
            $table->string('alamat_pembuat');
            $table->string('penanggungJawab_pembuat');
            $table->string('no_srut');
            $table->string('no_skrb');
            $table->string('tgl_skrb');
            $table->string('no_rangka');
            $table->string('no_mesin');
            $table->string('merek');
            $table->string('jenis_kendaraan');
            $table->string('warna_kendaraan');
            $table->string('lampiran');
            $table->string('qrCode');
            $table->timestamps();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datakendaraans');
    }
}
