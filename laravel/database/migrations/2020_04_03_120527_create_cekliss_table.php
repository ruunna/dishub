<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCeklissTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cekliss', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idceklis');
            $table->integer('idkendaraan')->unsigned();
            $table->string('kssuaian_lndsn');
            $table->string('kssuaian_bntk_fsk');
            $table->string('kssuaian_dmnsi_kndraan');
            $table->string('kssuaian_material');
            $table->string('kssuaian_pss_lmpu');
            $table->string('kssuaian_ukrn');
            $table->string('kssuaian_dmnsi_muatan');
            $table->string('kssuaian_vlm_muatan');
            $table->string('kssuaian_dmnsi_ddk');
            $table->string('kssuaian_jrk_ddk');
            $table->string('kssuaian_keluar_drrt');
            $table->timestamps();
            $table->foreign('idkendaraan')->references('idkendaraan')->on('datakendaraans')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cekliss');
    }
}
