<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDimensisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dimensis', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('iddimensi');
            $table->integer('idkendaraan')->unsigned();
            $table->integer('panjang_ttl');
            $table->integer('lebar_ttl');
            $table->integer('tinggi_ttl');
            $table->integer('jarak_smb12');
            $table->integer('rear_overhang');
            $table->integer('jarak_smb23');
            $table->integer('front_overhang');
            $table->integer('jarak_smb34');
            $table->timestamps();
            $table->foreign('idkendaraan')->references('idkendaraan')->on('datakendaraans')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dimensis');
    }
}
