<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBentukfisiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bentukfisiks', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idbentukfisik');
            $table->integer('idkendaraan')->unsigned();
            $table->string('img_tmpk_depan');
            $table->string('img_tmpk_blkng');
            $table->string('img_tmpk_smpng');
            $table->timestamps();
            $table->foreign('idkendaraan')->references('idkendaraan')->on('datakendaraans')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bentukfisiks');
    }
}
