<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKeterangansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('keterangans', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idketerangan');
            $table->integer('idkendaraan')->unsigned();
            $table->string('ukrn_smb1');
            $table->string('ukrn_smb2');
            $table->string('ukrn_smb3');
            $table->string('ukrn_smb4');
            $table->string('kekuatan_rancangan_smb1');
            $table->string('kekuatan_rancangan_smb2');
            $table->string('kekuatan_rancangan_smb3');
            $table->string('kekuatan_rancangan_smb4');
            $table->string('tnggi_box_bak_tang');
            $table->string('rmh_bdn_2');
            $table->string('knfg_smb');
            $table->string('kls_jln');
            $table->string('lbr_pntu_spr');
            $table->string('jrk_bngku');
            $table->string('brt_ksng');
            $table->string('dya_angkt_brng');
            $table->string('dya_angkt_pnmpng');
            $table->timestamps();
            $table->foreign('idkendaraan')->references('idkendaraan')->on('datakendaraans')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('keterangans');
    }
}
