<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKaroserisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('karoseris', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('idkaroseris');
            $table->integer('idkendaraan')->unsigned();
            $table->string('jmlh_silinder');
            $table->string('isi_silinder');
            $table->string('pjg_boxbak');
            $table->string('daya_mtr');
            $table->string('jmlh_tmpt_ddk');
            $table->string('tnggi_boxbaktang');
            $table->string('lbr_boxbaktang');
            $table->string('rmh_bdn');
            $table->string('jbb');
            $table->string('jbkb');
            $table->string('jns_angkutan');
            $table->string('jbi');
            $table->string('jbki');
            $table->string('bhan_bkr');
            $table->string('sudut_pgi');
            $table->string('pntu_darurat');
            $table->string('varian');
            $table->string('kt_baktang');
            $table->timestamps();
            $table->foreign('idkendaraan')->references('idkendaraan')->on('datakendaraans')->onDelete('cascade')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('karoseris');
    }
}
