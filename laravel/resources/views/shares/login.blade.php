@extends('shares.dashboard')
@section('content')
<div class="vertical-center">
    <div class="container box">
        <!-- Main Section -->
        <section class="main-section">
            <!-- Add Your Content Inside -->
            <div class="content">
                <!-- Remove This Before You Start -->
                <h1 style="color:white;">Admin -  Login</h1>
                <hr>
                @if(\Session::has('alert'))
                    <div class="alert alert-danger">
                        <div>{{Session::get('alert')}}</div>
                    </div>
                @endif
                @if(\Session::has('alert-success'))
                    <div class="alert alert-success">
                        <div>{{Session::get('alert-success')}}</div>
                    </div>
                @endif
                <form action="{{ url('/loginPost') }}" method="post">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label for="username" style="color:white;">Username:</label>
                        <input type="text" class="form-control" id="username" name="username">
                    </div>
                    <div class="form-group">
                        <label for="alamat" style="color:white;">Password:</label>
                        <input type="password" class="form-control" id="password" name="password"/>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-md btn-primary">Login</button>
                    </div>
                </form>
            </div>
            <!-- /.content -->
        </section>
        <!-- /.main-section -->
    </div>
</div>    
@endsection