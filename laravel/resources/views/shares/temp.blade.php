
<style>
  .uper {
    margin-top: 40px;
  }
</style>
<div class="uper">
  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div><br />
  @endif

  <!-- table Data kendaraan -->
  <table class="table table-striped">
    <thead>
        <tr>
          <td>No.Srut</td>
          <td>No.SK Rancang Bangun</td>
          <td>Tanggal SK Rancang Bangun</td>
          <td>Nomor Rangka</td>
          <td colspan="2">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($datakendaraan as $datakendaraan)
        <tr>
            <td>{{$datakendaraan->no_srut}}</td>
            <td>{{$datakendaraan->no_skrb}}</td>
            <td>{{$datakendaraan->tgl_skrb}}</td>
            <td><img src="{{URL::asset('/images/'.$datakendaraan->qrCode)}}"/></td>
            <td><a href="{{ route('shares.edit',$datakendaraan->idkendaraan)}}" class="btn btn-primary">Edit</a></td>
            <td>
                <form action="{{ route('shares.destroy', $datakendaraan->idkendaraan)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>