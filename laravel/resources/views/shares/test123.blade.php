
@extends('layout')

@section('content')
    <style>
      div.borders{
          text-align: center;
        }
      div.textL{

        text-align: left;
      }
      div.textC{

        text-align: center;
      }
      .uper {
        margin-top: 40px;
      }
      .row > div {
        border: 1px solid grey;
      }
      /* Create two equal columns that floats next to each other */
      .row{
            overflow: hidden;
        }

        [class*="col-"]{
            margin-bottom: -99999px;
            padding-bottom: 99999px;
        }
    </style>

<form onLoad="window.print()">
    <div class="row">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
              <h1 style="font-size:6vw;">Data Kendaraan</h1>
        </div>
        <div class="col-xs-12 textL" >
              <h2 style="font-size:4vw; ">No. Srut</h2>
              <p style="font-size:2vw;">{{$getData[0]->no_srut}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">No. SK Rancang Bangun</h2>
              <p style="font-size:2vw;">{{$getData[0]->no_skrb}}</p>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Tanggal SK Rancang Bangun</h2>
              <p style="font-size:2vw;">{{$getData[0]->tgl_skrb}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Nomor Rangka</h2>
              <p style="font-size:2vw;">{{$getData[0]->no_rangka}}</p>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Nomor Mesin</h2>
              <p style="font-size:2vw;">{{$getData[0]->no_mesin}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Merek</h2>
              <p style="font-size:2vw;">{{$getData[0]->merek}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Jenis Kendaraan</h2>
              <p style="font-size:2vw;">{{$getData[0]->jenis_kendaraan}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Warna</h2>
              <p style="font-size:2vw;">{{$getData[0]->warna_kendaraan}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">Lampiran</h2>
              <p style="font-size:2vw;">{{$getData[0]->lampiran}}</p>
        </div>
    </div>
    <br>
  <!-- 1. dimensi kendaraan -->
    <div class=" row ">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
              <h1 style="font-size:6vw;">1. DIMENSI KENDARAAN</h1>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">PANJANG TOTAL</h2>
              <p style="font-size:2vw;">{{$getData[0]->panjang_ttl}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">LEBAR TOTAL</h2>
              <p style="font-size:2vw;">{{$getData[0]->lebar_ttl}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">TINGGI TOTAL</h2>
              <p style="font-size:2vw;">{{$getData[0]->tinggi_ttl}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">JARAK SUMBU 1-2</h2>
              <p style="font-size:2vw;">{{$getData[0]->jarak_smb12}}</p>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">REAR OVERHANG (ROH)</h2>
              <p style="font-size:2vw;">{{$getData[0]->rear_overhang}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">JARAK SUMBU 2-3</h2>
              <p style="font-size:2vw;">{{$getData[0]->jarak_smb23}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">FRONT OVERHANG (FOH)</h2>
              <p style="font-size:2vw;">{{$getData[0]->front_overhang}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <h2 style="font-size:4vw; ">JARAK SUMBU 3-4</h2>
              <p style="font-size:2vw;">{{$getData[0]->jarak_smb34}}</p>
        </div>
    </div>
    <br>
  <!--2. karoseri -->
    <div class=" row ">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:6vw;">2. KAROSERI KENDARAAN</h2>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">JUMLAH SILINDER</h2>
            <p style="font-size:2vw;">{{$getData[0]->jmlh_silinder}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">ISI SILINDER</h2>
            <p style="font-size:2vw;">{{$getData[0]->isi_silinder}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">DAYA MOTOR</h2>
            <p style="font-size:2vw;">{{$getData[0]->daya_mtr}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">PANJANG BOX/BAK/ TANGKY</h2>
            <p style="font-size:2vw;">{{$getData[0]->pjg_boxbak}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">JUMLAH TEMPAT DUDUK</h2>
            <p style="font-size:2vw;">{{$getData[0]->jmlh_tmpt_ddk}}</p>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">TINGGI BOX/BAK/TANGKY</h2>
            <p style="font-size:2vw;">{{$getData[0]->tnggi_boxbaktang}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">LEBAR BOX/BAK/TANGKY</h2>
            <p style="font-size:2vw;">{{$getData[0]->lbr_boxbaktang}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">RUMAH BADAN</h2>
            <p style="font-size:2vw;">{{$getData[0]->rmh_bdn}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">JBB/JBKB</h2>
            <p style="font-size:2vw;">{{$getData[0]->jbb}}/{{$getData[0]->jbkb}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">JENIS ANGKUTAN</h2>
            <p style="font-size:2vw;">{{$getData[0]->jns_angkutan}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">JBI/JBKI</h2>
            <p style="font-size:2vw;">{{$getData[0]->jbi}}/{{$getData[0]->jbki}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">BAHAN BAKAR</h2>
            <p style="font-size:2vw;">{{$getData[0]->bhan_bkr}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">SUDUT PERIGI</h2>
            <p style="font-size:2vw;">{{$getData[0]->sudut_pgi}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">PINTU KELUAR DARURAT</h2>
            <p style="font-size:2vw;">{{$getData[0]->pntu_darurat}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">VARIAN</h2>
            <p style="font-size:2vw;">{{$getData[0]->varian}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">KETERANGAN BAK/TANGKI</h2>
            <p style="font-size:2vw;">{{$getData[0]->kt_baktang}}</p>
        </div>
    </div>
    <br>
<!--3. Bentuk fisik kendaraan -->
    <div class="row">
          <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:6vw;">3. BENTUK FISIK KENDARAAN</h2>
          </div>
          <div class="col-xs-12 textL" >
            <h2 style="font-size:4vw; ">TAMPAK DEPAN</h2>
            <img src="{{ URL::asset($getData[0]->img_tmpk_depan) }}" id="showgambar3" class="img-thumbnail"/>
          </div>
          <div class="col-xs-12 textL" >
            <h2 style="font-size:4vw; ">TAMPAK SAMPING</h2>
            <img src="{{ URL::asset($getData[0]->img_tmpk_smpng) }}" id="showgambar3" class="img-thumbnail"/>
          </div>
          <div class="col-xs-12 textL" >
            <h2 style="font-size:4vw; ">TAMPAK BELAKANG</h2>
            <img src="{{ URL::asset($getData[0]->img_tmpk_blkng) }}" id="showgambar3" class="img-thumbnail"/>
          </div>
    </div>

    <!--4. Keterangan -->
    <div class="row">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:6vw;">4. KETERANGAN KENDARAAN</h2>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">UKURAN BAN PADA SUMBU 1</h2>
            <p style="font-size:2vw; ">{{$getData[0]->ukrn_smb1}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">UKURAN BAN PADA SUMBU 2</h2>
            <p style="font-size:2vw; " >{{$getData[0]->ukrn_smb2}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">UKURAN BAN PADA SUMBU 3</h2>
            <p style="font-size:2vw; ">{{$getData[0]->ukrn_smb3}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">UKURAN BAN PADA SUMBU 4</h2>
            <p style="font-size:2vw; ">{{$getData[0]->ukrn_smb4}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">DIMENSI MUATAN DAN BAK TANGKI</h2>
            <p style="font-size:2vw; ">{{$getData[0]->tnggi_box_bak_tang}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">RUMAH BADAN</h2>
            <p style="font-size:2vw; ">{{$getData[0]->rmh_bdn_2}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">KONFIGURASI SUMBU</h2>
            <p style="font-size:2vw; ">{{$getData[0]->knfg_smb}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">KELAS JALAN</h2>
            <p style="font-size:2vw; ">{{$getData[0]->kls_jln}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">LEBAR PINTU SUPIR</h2>
            <p style="font-size:2vw; ">{{$getData[0]->lbr_pntu_spr}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">JARAK BANGKU</h2>
            <p style="font-size:2vw; ">{{$getData[0]->jrk_bngku}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">BERAT KOSONG(KG)</h2>
            <p style="font-size:2vw; ">{{$getData[0]->brt_ksng}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">DAYA ANGKUT BARANG(KG)</h2>
            <p style="font-size:2vw; ">{{$getData[0]->dya_angkt_brng}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">DAYA ANGKUT PENUMPANG(KG)</h2>
            <p style="font-size:2vw; ">{{$getData[0]->dya_angkt_pnmpng}}</p>
        </div>
    </div>
    <br>
    <!--5. CEKLIS KESESUAIAN DATA FISIKKENDARAAN DENGAN SK RANCANG BANGUN-->
    <div class=" row ">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:6vw;">5. CEKLIS KESESUAIAN DATA FISIKKENDARAAN DENGAN SK RANCANG BANGUN</h2>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">A. KESESUAIAN LANDASAN</h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_lndsn}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">B. KESESUAIAN BENTUK FISIK KENDARAAN </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_bntk_fsk}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">C. KESESUAIAN DIMENSI KENDARAAN </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_dmnsi_kndraan}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">D. KESESUAIAN MATERIAL</h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_material}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">E. KESESUAIAN POSISI LAMPU LAMPU </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_pss_lmpu}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">F. KESESUAIAN UKURAN BAN </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_ukrn}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">G. KESESUAIAN DIMENSI BAIK MUATAN </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_dmnsi_muatan}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">H. KESESUAIAN VOLUME BAK MUATAN </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_vlm_muatan}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">I. KESESUAIAN DIMENSI DAN JUMLAH TEMPAT DUDUK</h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_dmnsi_ddk}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">J. KESESUAIAN JARAK TEMPAT DUDUK </h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_jrk_ddk}}</p>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <h2 style="font-size:4vw; ">K. KESESUAIAN FASILITAS TEMPAT KELUAR DARUAT</h2>
            <p style="font-size:2vw;">{{$getData[0]->kssuaian_keluar_drrt}}</p>
        </div>
    </div>

<form>
@endsection
