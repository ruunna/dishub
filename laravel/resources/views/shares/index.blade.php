
@extends('dashboard')

@section('content')
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/material-design-lite/1.1.0/material.min.css" />
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/dataTables.material.min.css" />
<div class="card ">
  <div class="header">
      <h4 class="title">Data Srut</h4>
      <p class="category">List Semua Kendaraan</p>
  </div>
  <div class="content" style="overflow-x:auto;">
      <!-- table Data kendaraan -->
    <table id = "datakendaraan" class="table table-striped">
      <thead>
          <tr>
            <td>Nomor</td>
            <td>Id</td>
            <td>No.Srut</td>
            <td>No.SK Rancang Bangun</td>
            <td>Tanggal SK Rancang Bangun</td>
            <td>Nomor Rangka</td>
            <td>Action</td>
            <td></td>
          </tr>
      </thead>
      <tbody>
        <?php $no = 1;?>
          @foreach($datakendaraan as $datakendaraan)
          <tr>
              <td>{{$no++}}
              <td>{{$datakendaraan->idkendaraan}}</td>
              <td>{{$datakendaraan->no_srut}}</td>
              <td>{{$datakendaraan->no_skrb}}</td>
              <td>{{$datakendaraan->tgl_skrb}}</td>
              <td><img src="{{URL::asset('images/'.$datakendaraan->qrCode)}}" id="tabel"/></td>
              <td>
                  
                  <a class="btn btn-info" href="{{ route('detaildata',$datakendaraan->idkendaraan)}}">Detail</a>
              </td>
              <td>
                  <a href="{{ route('shares.edit',$datakendaraan->idkendaraan)}}" class="btn btn-primary">Edit</a>
              </td>
              <td>
                  <form action="{{ route('shares.destroy', $datakendaraan->idkendaraan)}}" method="post">
                    @csrf
                    @method('DELETE')
                    <button class="btn btn-danger" type="submit">Delete</button>
                  </form>
                  <!-- <a href="{{ route('printPDF',$datakendaraan->idkendaraan)}}" class="btn btn-primary">CETAK PDF</a> -->
              </td>
          </tr>
          @endforeach
      </tbody>
    </table>
  </div>
</div>
<script type="text/javascript">
  $(document).ready(function() {
    var table = $('#datakendaraan').DataTable({
      "bInfo": false,
      "bLengthChange": false
    });
} );
</script>
@endsection
@section('modal')
<script type="text/javascript">
    $('#datakendaraan').on('click', 'tr', function () {
        var name = $('td', this).eq(0).text();
        console.log(name);
        var url = "{{ url('datakendaraan/list/') }}/"+name;
        var fileloc = "{{URL::asset('images/')}}";
        $.get( url, function( data ) {
          console.log(data);
          $('.modal-body').empty();
          $('<div id="dialog_content"><h4>Data Kendaraan</h4></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>ID KENDARAAN     : '+data[0].idkendaraan+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO SRUT          : '+data[0].no_srut+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO SKRB          : '+data[0].no_skrb+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>TANGGAL SKRB     : '+data[0].tgl_skrb+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO RANGKA        : '+data[0].no_rangka+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO MESIN         : '+data[0].no_mesin+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>MEREK            : '+data[0].merek+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>JENIS_KENDARAAN  : '+data[0].jenis_kendaraan+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>WARNA KENDARAAN  : '+data[0].warna_kendaraan+'</li></div>').appendTo("#bodymodal");

          $('<div id="dialog_content"><hr>').appendTo('#bodymodal');          
          
          $('<div id="dialog_content"><h4>Bentuk Fisik Kendaraan</h4></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>ID KENDARAAN     : <img src="'+fileloc+'/'+data[0].qrCode+'"/></li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO SRUT          : '+data[0].no_srut+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO SKRB          : '+data[0].no_skrb+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>TANGGAL SKRB     : '+data[0].tgl_skrb+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO RANGKA        : '+data[0].no_rangka+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>NO MESIN         : '+data[0].no_mesin+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>MEREK            : '+data[0].merek+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>JENIS_KENDARAAN  : '+data[0].jenis_kendaraan+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><li>WARNA KENDARAAN  : '+data[0].warna_kendaraan+'</li></div>').appendTo("#bodymodal");
          $('<div id="dialog_content"><hr>').appendTo('#bodymodal');          
          $('#DescModal').modal("show");
        });
    });
    
</script>
@endsection

