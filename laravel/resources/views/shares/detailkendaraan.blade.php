@extends('dashboard')

@section('content')
    <style>
        hr {
            display: block; height: 1px;
            border: 0; border-top: 1px solid #ccc;
            margin: 1em 0; padding: 0;
        }
        .modal-backdrop {
        z-index: -1;
        }
      div.borders{
          text-align: center;
        }
      div.textL{

        text-align: left;
      }
      div.textC{

        text-align: center;
      }
      .uper {
        margin-top: 40px;
      }
      .row > div {
        border: 1px grey;
      }
      /* Create two equal columns that floats next to each other */
      .row{
            overflow: hidden;
        }

        [class*="col-"]{
            margin-bottom: -99999px;
            padding-bottom: 99999px;
        }
    </style>
        <div style="text-align: center">
            <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                            ->encoding('UTF-8')
                            ->backgroundColor(255, 255, 255, 100)
                            ->size(200)
                            ->margin(0)
                            ->generate('https://ujitipeditsrana.id/shares/datakendaraan/idkendaraan='.$getData[0]->idkendaraan)) !!} ">
        </div>
        <div class=" textC p-3 mb-2 bg-primary text-white" >
              <h1 style="font-size:16px; display: list-item;">DATA PEMILIK</h1>
        </div>
        <div class="col-xs-12 textL" >
              <p style="font-size:16px; display: list-item;">Perusahaan Pembuat</p>
              <p id="perusahaan_pembuat" style="font-size:14px; font-style: oblique; text-color:#D3D3D3; " class="text-light">{{$getData[0]->perusahaan_pembuat}}</p>
              <hr>
        </div>
        <div class="col-xs-12 textL" >
              <p style="font-size:16px; display: list-item;">Alamat Perusahaan Pembuat</p>
              <p id="alamat_pembuat" style="font-size:14px; font-style: oblique; text-color:#D3D3D3; " class="text-light">{{$getData[0]->alamat_pembuat}}</p>
              <hr>
        </div>
        <div class="col-xs-12 textL" >
              <p style="font-size:16px; display: list-item;">Penanggung Jawab Perusahaan </p>
              <p id="penanggungJwb_pembuat" style="font-size:14px; font-style: oblique; text-color:#D3D3D3; " class="text-light">{{$getData[0]->penanggungJwb_pembuat}}</p>
              <hr>
        </div>
    </div>
      <br>
    <div class="row">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
              <h1 style="font-size:16px; display: list-item;">DATA KENDARAAN</h1>
        </div>
        <div class="col-xs-12 textL" >
              <p style="font-size:16px; display: list-item;">No. Srut</p>
              <p id="NoSrut" style="font-size:14px; font-style: oblique; text-color:#D3D3D3; " class="text-light">{{$getData[0]->no_srut}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">No. SK Rancang Bangun</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->no_skrb}}</p>
              <hr>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Tanggal SK Rancang Bangun</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->tgl_skrb}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Nomor Rangka</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->no_rangka}}</p>
              <hr>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Nomor Mesin</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->no_mesin}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Merek</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->merek}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Jenis Kendaraan</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jenis_kendaraan}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Warna</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->warna_kendaraan}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">Lampiran</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->lampiran}}</p>
              <hr>
        </div>
    </div>
    <br>
  <!-- 1. dimensi kendaraan -->
    <div class=" row ">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
              <h1 style="font-size:16px; display: list-item;">1. DIMENSI KENDARAAN</h1>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">PANJANG TOTAL</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->panjang_ttl}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">LEBAR TOTAL</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->lebar_ttl}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">TINGGI TOTAL</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->tinggi_ttl}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">JARAK SUMBU 1-2</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jarak_smb12}}</p>
              <hr>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">REAR OVERHANG (ROH)</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->rear_overhang}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">JARAK SUMBU 2-3</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jarak_smb23}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">FRONT OVERHANG (FOH)</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->front_overhang}}</p>
              <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
              <p style="font-size:16px; display: list-item;">JARAK SUMBU 3-4</p>
              <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jarak_smb34}}</p>
              <hr>
        </div>
    </div>
    <br>
  <!--2. karoseri -->
    <div class=" row ">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:16px; display: list-item;">2. KAROSERI KENDARAAN</h1>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">JUMLAH SILINDER</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jmlh_silinder}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">ISI SILINDER</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->isi_silinder}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">DAYA MOTOR</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->daya_mtr}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">PANJANG BOX/BAK/ TANGKY</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->pjg_boxbak}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">JUMLAH TEMPAT DUDUK</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jmlh_tmpt_ddk}}</p>
            <hr>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">TINGGI BOX/BAK/TANGKY</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->tnggi_boxbaktang}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">LEBAR BOX/BAK/TANGKY</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->lbr_boxbaktang}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">RUMAH BADAN</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->rmh_bdn}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">JBB/JBKB</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jbb}}/{{$getData[0]->jbkb}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">JENIS ANGKUTAN</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jns_angkutan}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">JBI/JBKI</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jbi}}/{{$getData[0]->jbki}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">BAHAN BAKAR</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->bhan_bkr}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">SUDUT PERIGI</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->sudut_pgi}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">PINTU KELUAR DARURAT</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->pntu_darurat}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">VARIAN</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->varian}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KETERANGAN BAK/TANGKI</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kt_baktang}}</p>
            <hr>
        </div>
    </div>
    <br>
<!--3. Bentuk fisik kendaraan -->
    <div class="row">
          <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:16px; display: list-item;">3. BENTUK FISIK KENDARAAN</h1>
          </div>

          <div class="col-xs-12 textL" >
            <br>
            <p style="font-size:16px; display: list-item;">TAMPAK DEPAN</p>
            <img src="{{ URL::asset($getData[0]->img_tmpk_depan)}}" id="showgambar3" class="img-thumbnail"/>
          </div>

          <div class="col-xs-12 textL" >
            <br>
            <p style="font-size:16px; display: list-item;">TAMPAK SAMPING</p>
            <img src="{{ URL::asset($getData[0]->img_tmpk_smpng) }}" id="showgambar3" class="img-thumbnail"/>
          </div>

          <div class="col-xs-12 textL" >
            <br>
            <p style="font-size:16px; display: list-item;">TAMPAK BELAKANG</p>
            <img src="{{ URL::asset($getData[0]->img_tmpk_blkng) }}" id="showgambar3" class="img-thumbnail"/>
          </div>
          <br>

    </div>
    <br>
    <br>
    <!--4. Keterangan -->
    <div class="row">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:14px; ">4. KETERANGAN KENDARAAN</h1>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">UKURAN BAN PADA SUMBU 1</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->ukrn_smb1}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">UKURAN BAN PADA SUMBU 2</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->ukrn_smb2}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">UKURAN BAN PADA SUMBU 3</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->ukrn_smb3}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">UKURAN BAN PADA SUMBU 4</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->ukrn_smb4}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KEKUATAN RANCANGAN SUMBU 1</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kekuatan_rancangan_smb1}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KEKUATAN RANCANGAN SUMBU 2</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kekuatan_rancangan_smb2}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KEKUATAN RANCANGAN SUMBU 3</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kekuatan_rancangan_smb3}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KEKUATAN RANCANGAN SUMBU 4</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kekuatan_rancangan_smb4}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">DIMENSI MUATAN DAN BAK TANGKI</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->tnggi_box_bak_tang}}</p>
            <hr>
        </div>

        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">RUMAH BADAN</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->rmh_bdn_2}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KONFIGURASI SUMBU</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->knfg_smb}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">KELAS JALAN</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kls_jln}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">LEBAR PINTU SUPIR</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->lbr_pntu_spr}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">JARAK BANGKU</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->jrk_bngku}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">BERAT KOSONG(KG)</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->brt_ksng}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">DAYA ANGKUT BARANG(KG)</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->dya_angkt_brng}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px; display: list-item;">DAYA ANGKUT PENUMPANG(KG)</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->dya_angkt_pnmpng}}</p>
            <hr>
        </div>
    </div>
    <br>
    <!--5. CEKLIS KESESUAIAN DATA FISIKKENDARAAN DENGAN SK RANCANG BANGUN-->
    <div class=" row ">
        <div class=" textC p-3 mb-2 bg-primary text-white" >
            <h1 style="font-size:16px; display: list-item;">5. CEKLIS KESESUAIAN DATA FISIKKENDARAAN DENGAN SK RANCANG BANGUN</h1>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">A. KESESUAIAN LANDASAN</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_lndsn}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">B. KESESUAIAN BENTUK FISIK KENDARAAN </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_bntk_fsk}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">C. KESESUAIAN DIMENSI KENDARAAN </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_dmnsi_kndraan}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">D. KESESUAIAN MATERIAL</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_material}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">E. KESESUAIAN POSISI LAMPU LAMPU </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_pss_lmpu}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">F. KESESUAIAN UKURAN BAN </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_ukrn}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">G. KESESUAIAN DIMENSI BAIK MUATAN </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_dmnsi_muatan}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">H. KESESUAIAN VOLUME BAK MUATAN </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_vlm_muatan}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">I. KESESUAIAN DIMENSI DAN JUMLAH TEMPAT DUDUK</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_dmnsi_ddk}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">J. KESESUAIAN JARAK TEMPAT DUDUK </p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_jrk_ddk}}</p>
            <hr>
        </div>
        <div class="col-xs-6 textL p-3 mb-2 bg-light text-dark" >
            <p style="font-size:16px;">K. KESESUAIAN FASILITAS TEMPAT KELUAR DARUAT</p>
            <p style="font-size:14px; font-style: oblique; text-color:#D3D3D3;" class="text-light">{{$getData[0]->kssuaian_keluar_drrt}}</p>
            <hr>
        </div>
        <br>
            <!-- <a href="{{ route('printPDF',$getData[0]->idkendaraan)}}" class="btn btn-primary">CETAK PDF</a>
            <button class="btn btn-danger" type="submit">Delete</button> -->
    </div>
        <br>
        <br>
        <br>

        <div class='text-center'>
            <button class="btn btn-primary" id="ajaxSubmit" onClick="getform({{$getData[0]->idkendaraan}})" style="width: 10%;">Print</button>
        </div>

<!-- modal input  -->
<div class="modal fade" id="_form" tabindex="-1" role="dialog" aria-labelledby="purchaseLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="purchaseLabel">Input pengesahan</h4>
                </div>
                <div class="modal-body" style=" margin-left:20px; margin-right:20px;">
                    <form class="form-horizontal"  name="formfiltered" id="formfiltered"  >
                    {{ csrf_field() }}
                    <div class="form-group">
                        <!-- <label type="hidden">IDKendaraan</label> -->
                        <input type="hidden" class="form-control" name="IDKendaraan" id="IDKendaraan" >
                    </div>
                    <div class="form-group">
                        <label>Direktur Sarana Transportasi Jalan</label>
                        <input type="text" class="form-control" name="dirutSarana" id="dirutSarana">
                    </div>
                    <div class="form-group">
                        <label>NIP Direktur Sarana Perhubungan Darat</label>
                        <input type="text" class="form-control" name="NIPdirutSarana" id="NIPdirutSarana">
                    </div>
                    <div class="form-group">
                        <label>Pengesah</label>
                        <input type="text" class="form-control" name="pengesah" id="pengesah">
                    </div>
                    <div class="form-group">
                        <label>Jabatan Pengesah</label>
                        <input type="text" class="form-control" name="jabatanPengesah" id="jabatanPengesah">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengesah</label>
                        <input type="text" class="form-control" name="tanggalPengesah" id="tanggalPengesah">
                    </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary" onCLick="printSrt()">Print</button>
                </div>
            </div>
        </div>
 </div>
<!-- end modal input -->

@endsection
