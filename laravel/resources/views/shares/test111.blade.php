@extends('layout')

@section('content')
    <main id="main">
 <section id="pendaftaran" class="jumbotron" style="background:#FFF !important">
    <div class="container">
<br>
<br>
<hr>
        <header class="section-header">
          <h3>DATA SRUT</h3>
        </header>

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<form method="POST" action="http://ujitiperb.dephub.go.id/" accept-charset="UTF-8" class="form-horizontal form-material" id="form_uji" enctype="multipart/form-data">

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<hr>
							<h5><strong style="color:#306cba !important;"> DATA KENDARAAN : </strong></h5>
							<hr>
									<table class="table table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">No. SRUT<br>
													<span class="text-success"> {{$getData[0]->no_srut}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">
													 &nbsp;
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">No. SK Rancang Bangun<br>
													<span class="text-success">{{$getData[0]->no_skrb}}</span>
												</td>
												<td width="50%" class="text-uppercase text-info">{{$getData[0]->tgl_skrb}}<br>
													<span class="text-success"> 06 - 11 - 2019 </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Nomor Rangka<br>
													<span class="text-success"> {{$getData[0]->no_rangka}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Nomor Mesin<br>
													<span class="text-success"> {{$getData[0]->no_mesin}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info"> Merek <br>
													<span class="text-success">{{$getData[0]->merek}}</span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jenis Kendaraan<br>
													<span class="text-success">{{$getData[0]->jenis_kendaraan}}</span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Warna<br>
													<span class="text-success"> {{$getData[0]->warna_kendaraan}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Lampiran<br>
													<div class="btn btn-sm btn-success" data-toggle="modal" data-target="#lampirankendaraan"><span class="btn-label"><i class="ti-file"></i></span>Lihat Lampiran Kendaraan</div>
													<div class="modal fade" id="lampirankendaraan" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
														<div class="modal-dialog  modal-lg">
															<div class="modal-content">
																<div class="modal-header">
																	LAMPIRAN DOKUMEN KENDARAAN
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
																</div>
																<div class="modal-body form">
																	<iframe style="width:100%; height:500px" src="cid:frame-9039E2743381CAA03262B73EE0409629@mhtml.blink"></iframe>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="btn-label"><i class="fa fa-close"></i></span>Keluar</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
							<hr>
							<h5><strong style="color:#306cba !important;">1. DIMENSI KENDARAAN :</strong></h5>
							<hr>
									<table class="table table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">Panjang Total<br>
													<span class="text-success"> {{$getData[0]->panjang_ttl}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Lebar Total<br>
													<span class="text-success"> {{$getData[0]->lebar_ttl}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Tinggi Total<br>
													<span class="text-success"> {{$getData[0]->tinggi_ttl}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Sumbu 1 - 2<br>
													<span class="text-success"> {{$getData[0]->jarak_smb12}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Rear Overhang (ROH)<br>
													<span class="text-success"> {{$getData[0]->rear_overhang}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Sumbu 2 - 3<br>
													<span class="text-success"> {{$getData[0]->jarak_smb23}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Front Overhang (FOH)<br>
													<span class="text-success"> {{$getData[0]->front_overhang}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Sumbu 3 - 4<br>
													<span class="text-success"> {{$getData[0]->jarak_smb34}} </span>
												</td>
											</tr>
										</tbody>
									</table>
							<hr>
							<h5><strong style="color:#306cba !important;">2. KAROSERI KENDARAAN :</strong></h5>
							<hr>
									<table class="table  table-striped table-bordered">
										<tbody>

											<tr>
												<td width="50%" class="text-uppercase text-info">Jumlah Silinder<br>
													<span class="text-success"> {{$getData[0]->jmlh_silinder}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Isi Silinder<br>
													<span class="text-success"> {{$getData[0]->isi_silinder}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Panjang Box / Bak / Tangki<br>
													<span class="text-success"> {{$getData[0]->pjg_boxbak}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Daya Motor<br>
													<span class="text-success"> {{$getData[0]->daya_mtr}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Lebar Box / Bak / Tangki<br>
													<span class="text-success"> {{$getData[0]->lbr_boxbaktang}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jumlah Tempat Duduk<br>
													<span class="text-success"> {{$getData[0]->jmlh_tmpt_ddk}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Tinggi Box / Bak / Tangki<br>
													<span class="text-success"> {{$getData[0]->tnggi_boxbaktang}} </span>
												</td>

												<td width="50%" class="text-uppercase text-info">Rumah Badan<br>
													<span class="text-success"> {{$getData[0]->rmh_bdn}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">JBB/JBKB<br>
													<span class="text-success"> {{$getData[0]->jbb}}/{{$getData[0]->jbkb}}  </span>
												</td>

												<td width="50%" class="text-uppercase text-info">Jenis Angkutan<br>
													<span class="text-success"> {{$getData[0]->jns_angkutan}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">JBI/JBKI<br>
													<span class="text-success"> {{$getData[0]->jbi}}/{{$getData[0]->jbki}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Bahan Bakar<br>
													<span class="text-success"> {{$getData[0]->bhan_bkr}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Sudut Pergi<br>
													<span class="text-success"> {{$getData[0]->sudut_pgi}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Pintu Keluar Darurat<br>
													<span class="text-success"> {{$getData[0]->pntu_darurat}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Varian<br>
													<span class="text-success"> {{$getData[0]->varian}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Keterangan Bak/Tangki<br>
													<span class="text-success">{{$getData[0]->kt_baktang}}</span>
												</td>

											</tr>
										</tbody>
									</table>
							<hr>
							<h5><strong style="color:#306cba !important;">3. BENTUK FISIK KENDARAAN :</strong></h5>
							<hr>
												<div class="row col-md-12">
													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
													<img src="{{ URL::asset($getData[0]->img_tmpk_depan) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
													 <span class="text-uppercase text-info"> Tampak Depan </span>
													</div>

													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
													<img src="{{ URL::asset($getData[0]->img_tmpk_smpng) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
													 <span class="text-uppercase text-info"> Tampak Samping </span>
													</div>

													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
													<img src="{{ URL::asset($getData[0]->img_tmpk_blkng) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
													<span class="text-uppercase text-info"> Tampak Belakang </span>
													</div>
												</div>

							<hr>
							<h5><strong style="color:#306cba !important;">4. KETERANGAN :</strong></h5>
							<hr>
									  <table class="table  table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 1<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb1}}</span>
												</td>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 2<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb2}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 3<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb3}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 4<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb4}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 1<br>
													<span class="text-success"> 2645 </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 2<br>
													<span class="text-success"> 5355 </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 3<br>
													<span class="text-success"> 0 </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 4<br>
													<span class="text-success"> 0 </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Konfigurasi Sumbu<br>
													<span class="text-success"> {{$getData[0]->knfg_smb}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Kelas Jalan<br>
													<span class="text-success"> {{$getData[0]->kls_jln}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Lebar Pintu Supir<br>
													<span class="text-success"> {{$getData[0]->lbr_pntu_spr}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Bangku<br>
													<span class="text-success"> {{$getData[0]->jrk_bngku}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Berat Kosong (Kg)<br>
													<span class="text-success"> {{$getData[0]->brt_ksng}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Daya Angkut Barang (Kg)<br>
													<span class="text-success"> {{$getData[0]->dya_angkt_brng}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Daya Angkut Penumpang (Kg)<br>
													<span class="text-success"> {{$getData[0]->dya_angkt_pnmpng}} </span>
												</td>
											</tr>
										</tbody>
									</table>

							<hr>
							<h5><strong style="color:#306cba !important;">5. CEKLIST KESESUAIAN DATA FISIK KENDARAAN DENGAN SK RANCANG BANGUN :</strong></h5>
							<hr>
									  <table class="table  table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">a. Kesesuaian Landasan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_lndsn}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">g. Kesesuaian Dimensi Bak Muatan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_dmnsi_muatan}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">b. Kesesuaian Bentuk Fisik Kendaraan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_bntk_fsk}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">h. Kesesuaian Volume Bak Muatan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_vlm_muatan}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">c. Kesesuaian Dimensi Kendaraan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_dmnsi_kndraan}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">i. Kesesuaian Dimensi dan Jumlah Tempat Duduk<br>
													<span class="text-success"> {{$getData[0]->kssuaian_dmnsi_ddk}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">d. Kesesuaian Material<br>
													<span class="text-success"> {{$getData[0]->kssuaian_material}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">j. Kesesuaian Jarak Tempat Duduk<br>
													<span class="text-success"> {{$getData[0]->kssuaian_jrk_ddk}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">e. Kesesuaian Posisi Lampu - Lampu<br>
													<span class="text-success"> {{$getData[0]->kssuaian_pss_lmpu}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">k. Kesesuaian Fasilitas Tempat Keluar Darurat<br>
													<span class="text-success"> {{$getData[0]->kssuaian_keluar_drrt}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">f. Kesesuaian Ukuran Ban<br>
													<span class="text-success"> {{$getData[0]->kssuaian_ukrn}} </span>
												</td>
											</tr>
										</tbody>
									</table>
						</div>
					</div>
				</div>
				<hr>
				<div class="text-center">
				<a href="http://ujitiperb.dephub.go.id/" class="btn btn-info"><span><i class="fa fa-fw fa-home"></i></span> Home</a>
				</div>
			</form>
			</div>
		</div>
	</div>
	</div>

</section>
</main>
@endsection
