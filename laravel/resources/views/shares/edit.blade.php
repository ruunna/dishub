@extends('dashboard')

@section('content')
<style>
  .uper {
    margin-top: 40px;
    
  }
  input{
    text-transform: uppercase;
  }
</style>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- @if ($message = Session::get('success'))
      <div class="alert alert-success alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button> 
          <strong>{{ $message }}</strong>
      </div>
    @endif -->
<div class="card col-md-12">
  <div class="header" style="atext-align: center;">
    <h1 class="title" >Edit Data Srut</h1>
  </div>
  <div class="content">
    <form method="post" action="{{ route('shares.update', $datakendaraan[0]->idkendaraan) }}" enctype="multipart/form-data">  
      @method('PATCH')
        @csrf
        <div class="row">
            <h4 class="title"><b>DATA PEMBUAT</b></h4>
            <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Perusahaan Pembuat:</label>
                <input type="text" class="form-control" name="nama_pembuat" value='{{$datakendaraan[0]->perusahaan_pembuat}}' />
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Alamat Pembuat:</label>
                <input type="text" class="form-control" name="alamat_pembuat" value='{{$datakendaraan[0]->alamat_pembuat}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Penanggung Jawab Pembuat:</label>
                <input type="text" class="form-control" name="penanggungJwb_pembuat" value='{{$datakendaraan[0]->penanggungJwb_pembuat}}' />
            </div>
          </div>
        <div class="row">
            <h4 class="title"><b>DATA SRUT</b></h4>
            <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">No. Srut:</label>
                <input type="text" class="form-control" name="no_srut" value='{{$datakendaraan[0]->no_srut}}' />
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">No. SK Rancang Bangun:</label>
                <input type="text" class="form-control" name="no_skrb" value='{{$datakendaraan[0]->no_skrb}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Tanggal SK Rancang Bangun:</label>
                <input type="text" class="form-control" name="tgl_skrb" value='{{$datakendaraan[0]->tgl_skrb}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Nomor Rangka:</label>
                <input type="text" class="form-control" name="no_rangka" value='{{$datakendaraan[0]->no_rangka}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Nomor Mesin:</label>
                <input type="text" class="form-control" name="no_mesin" value='{{$datakendaraan[0]->no_mesin}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Merek:</label>
                <input type="text" class="form-control" name="merek" value='{{$datakendaraan[0]->merek}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jenis Kendaraan:</label>
                <input type="text" class="form-control" name="jenis_kendaraan" value='{{$datakendaraan[0]->jenis_kendaraan}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Warna Kendaraan:</label>
                <input type="text" class="form-control" name="warna_kendaraan" value='{{$datakendaraan[0]->warna_kendaraan}}'/>
            </div><div class="form-group">
                {{ csrf_field() }}
                <label for="name">Lampiran:</label>
                <input type="text" class="form-control" name="lampiran" value='{{$datakendaraan[0]->lampiran}}'/>
            </div>
          </div>
            <br>
          <!-- 1.Dimensi kendaraan -->
          <div class="row">
            <h4 class="title"><b> 1. DATA KENDARAAN</b></h4>
            <br>    
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Panjang Total:</label>
                <input type="text" class="form-control" name="panjang_ttl" value='{{$datakendaraan[0]->panjang_ttl}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Lebar Total:</label>
                <input type="text" class="form-control" name="lebar_ttl" value='{{$datakendaraan[0]->lebar_ttl}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Tinggi Total:</label>
                <input type="text" class="form-control" name="tinggi_ttl" value='{{$datakendaraan[0]->tinggi_ttl}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jarak Sumbu 1-2:</label>
                <input type="text" class="form-control" name="jarak_smb12" value='{{$datakendaraan[0]->jarak_smb12}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Rear Overhang(ROH):</label>
                <input type="text" class="form-control" name="rear_overhang" value='{{$datakendaraan[0]->rear_overhang}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jarak Sumbu 2-3:</label>
                <input type="text" class="form-control" name="jarak_smb23" value='{{$datakendaraan[0]->jarak_smb23}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Front Overhang(FOH):</label>
                <input type="text" class="form-control" name="front_overhang" value='{{$datakendaraan[0]->front_overhang}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jarak Sumbu 3-4:</label>
                <input type="text" class="form-control" name="jarak_smb34" value='{{$datakendaraan[0]->jarak_smb34}}'/>
            </div>
          </div>
            <br>
            <br>
            <!-- 2. Karoseri Kendaraan -->
          <div class="row">
            <h4 class="title"><b> 2. KAROSERI KENDARAAN</b></h4>
            <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jumlah Silinder:</label>
                <input type="text" class="form-control" name="jmlh_silinder" value='{{$datakendaraan[0]->jmlh_silinder}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Isi Silinder:</label>
                <input type="text" class="form-control" name="isi_silinder" value='{{$datakendaraan[0]->isi_silinder}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Panjang Box/Bak:</label>
                <input type="text" class="form-control" name="pjg_boxbak" value='{{$datakendaraan[0]->pjg_boxbak}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Daya Motor:</label>
                <input type="text" class="form-control" name="daya_mtr" value='{{$datakendaraan[0]->daya_mtr}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jumlah tempat duduk:</label>
                <input type="text" class="form-control" name="jmlh_tmpt_ddk" value='{{$datakendaraan[0]->jmlh_tmpt_ddk}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">tinggi Box/Bak/Tangki</label>
                <input type="text" class="form-control" name="tnggi_boxbaktang" value='{{$datakendaraan[0]->tnggi_boxbaktang}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Lebar Box/Bak/Tangki:</label> 
                <input type="text" class="form-control" name="lbr_boxbaktang" value='{{$datakendaraan[0]->lbr_boxbaktang}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Rumah Badan:</label>
                <input type="text" class="form-control" name="rmh_bdn" value='{{$datakendaraan[0]->rmh_bdn}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">JBB/JBKB:</label>
                <input type="text" class="form-control" name="jbb" value='{{$datakendaraan[0]->jbb}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">JBKB:</label>
                <input type="text" class="form-control" name="jbkb" value='{{$datakendaraan[0]->jbkb}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Jenis Angkutan:</label>
                <input type="text" class="form-control" name="jns_angkutan" value='{{$datakendaraan[0]->jns_angkutan}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">JBI:</label>
                <input type="text" class="form-control" name="jbi" value='{{$datakendaraan[0]->jbi}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">JBKI:</label>
                <input type="text" class="form-control" name="jbki" value='{{$datakendaraan[0]->jbki}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Bahan Bakar:</label>
                <input type="text" class="form-control" name="bhan_bkr" value='{{$datakendaraan[0]->bhan_bkr}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Sudut Perigi:</label>
                <input type="text" class="form-control" name="sudut_pgi" value='{{$datakendaraan[0]->sudut_pgi}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Pintu Keluar Darurat:</label>
                <input type="text" class="form-control" name="pntu_darurat" value='{{$datakendaraan[0]->pntu_darurat}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Varian:</label>
                <input type="text" class="form-control" name="varian" value='{{$datakendaraan[0]->varian}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Keterangan BAK/Tangki:</label>
                <input type="text" class="form-control" name="kt_baktang" value='{{$datakendaraan[0]->kt_baktang}}'/>
            </div>
          </div>
            <br>
            <br>
          <!-- 3. Bentuk Fisik Kendaraan -->
          <div class="row">
            <h4 class="title"><b> 3. BENTUK FISIK KENDARAAN</b></h4>
            <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Tampak Depan</label>
                
                <br>
                    <img src="{{ URL::asset($datakendaraan[0]->img_tmpk_depan) }}" id="showgambar1" style="max-width:200px;max-height:200px;float:left;"/>
                    <input type='text' name="img_tmpk_depan1" value="{{$datakendaraan[0]->img_tmpk_depan}}" hidden>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <input type="file" id="img_tmpk_depan" name="img_tmpk_depan" value='{{$datakendaraan[0]->img_tmpk_depan}}' class="validate"  />

            </div>
                <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Tampak Samping</label>
                <br>
                <img src="{{ URL::asset($datakendaraan[0]->img_tmpk_smpng) }}" id="showgambar2" style="max-width:200px;max-height:200px;float:left;"/>
                <input type='text' name="img_tmpk_smpng2" value="{{$datakendaraan[0]->img_tmpk_smpng}}" hidden>
                <br>
                <br>
                <br>
                <br>
                <br>    
                <br>
                <br>
                <br>
                <input type="file" id="img_tmpk_smpng" name="img_tmpk_smpng" class="validate" value="{{$datakendaraan[0]->img_tmpk_smpng}}"/>
            
            </div>
                <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">Tampak Belakang</label>
                <br>
                <img src="{{ URL::asset($datakendaraan[0]->img_tmpk_blkng) }}" id="showgambar3" style="max-width:200px;max-height:200px;float:left;"/>
                <input type='text' name="img_tmpk_blkng3" value="{{$datakendaraan[0]->img_tmpk_blkng}}" hidden>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <input type="file" id="img_tmpk_blkng" name="img_tmpk_blkng" class="validate" value='{{$datakendaraan[0]->img_tmpk_blkng}}'/>
            </div>
          </div>
            <br>
            <br>
          <!-- 4. Keterangan -->
          <div class="row">
            <h4 class="title"><b> 4. KETERANGAN</b></h4>
            <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">UKURAN BAN PADA SUMBU 1</label>
                <input type="text" class="form-control" name="ukrn_smb1" value='{{$datakendaraan[0]->ukrn_smb1}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">UKURAN BAN PADA SUMBU 2</label>
                <input type="text" class="form-control" name="ukrn_smb2" value='{{$datakendaraan[0]->ukrn_smb2}}'/>
            </div>
            <div class="form-grou/p">
                {{ csrf_field() }}
                <label for="name">UKURAN BAN PADA SUMBU 3</label>
                <input type="text" class="form-control" name="ukrn_smb3" value='{{$datakendaraan[0]->ukrn_smb3}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">UKURAN BAN PADA SUMBU 4</label>
                <input type="text" class="form-control" name="ukrn_smb4" value='{{$datakendaraan[0]->ukrn_smb4}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">KEKUATAN RANCANGAN SUMBU 1</label>
                <input type="text" class="form-control" name="kekuatan_rancangan_smb1" value='{{$datakendaraan[0]->kekuatan_rancangan_smb1}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">KEKUATAN RANCANGAN SUMBU 2</label>
                <input type="text" class="form-control" name="kekuatan_rancangan_smb2" value='{{$datakendaraan[0]->kekuatan_rancangan_smb2}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">KEKUATAN RANCANGAN SUMBU 3</label>
                <input type="text" class="form-control" name="kekuatan_rancangan_smb3" value='{{$datakendaraan[0]->kekuatan_rancangan_smb3}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">KEKUATAN RANCANGAN SUMBU 4</label>
                <input type="text" class="form-control" name="kekuatan_rancangan_smb4" value='{{$datakendaraan[0]->kekuatan_rancangan_smb4}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">TINGGI BOX/BAK/TANGKY</label>
                <input type="text" class="form-control" name="tnggi_box_bak_tang" value='{{$datakendaraan[0]->tnggi_box_bak_tang}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">RUMAH BADAN</label>
                <input type="text" class="form-control" name="rmh_bdn_2" value='{{$datakendaraan[0]->rmh_bdn_2}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">KONFIGURASI SUMBU</label>
                <input type="text" class="form-control" name="knfg_smb" value='{{$datakendaraan[0]->knfg_smb}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">KELAS JALAN</label>
                <input type="text" class="form-control" name="kls_jln" value='{{$datakendaraan[0]->kls_jln}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">LEBAR PINTU SUPIR</label> 
                <input type="text" class="form-control" name="lbr_pntu_spr" value='{{$datakendaraan[0]->lbr_pntu_spr}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">JARAK BANGKU</label>
                <input type="text" class="form-control" name="jrk_bngku" value='{{$datakendaraan[0]->jrk_bngku}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">BERAT KOSONG (KG)</label>
                <input type="text" class="form-control" name="brt_ksng" value='{{$datakendaraan[0]->brt_ksng}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">DAYA ANGKUT BARANG (KG)</label>
                <input type="text" class="form-control" name="dya_angkt_brng" value='{{$datakendaraan[0]->dya_angkt_brng}}'/>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">DAYA ANGKUT PENUMPANG (KG)</label>
                <input type="text" class="form-control" name="dya_angkt_pnmpng" value='{{$datakendaraan[0]->dya_angkt_pnmpng}}'/>
            </div>
          </div>
            <br>
            <br>
          <!-- 5. Ceklis kesesuaian data fisik dengan data kendaraan  dengan SK rancang Bangun -->
          <div class="row">
            <h4 class="title"><b> 5. CEKLIS KESESUAIAN DATA FISIKKENDARAAN DENGAN SK RANCANG BANGUN</b></h4>
            <br>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">A. KESESUAIAN LANDASAN</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_lndsn" value="Sesuai" {{ $datakendaraan[0]->kssuaian_lndsn== 'Sesuai' ? 'checked' : ''}}/><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_lndsn" value="Tidak Sesuai"{{ $datakendaraan[0]->kssuaian_lndsn== 'Tidak Sesuai' ? 'checked' : ''}}/><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">B. KESESUAIAN BENTUK FISIK KENDARAAN</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_bntk_fsk" value="Sesuai" {{ $datakendaraan[0]->kssuaian_bntk_fsk== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_bntk_fsk" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_bntk_fsk== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">C. KESESUAIAN DIMENSI KENDARAAN</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_dmnsi_kndraan" value="Sesuai" {{ $datakendaraan[0]->kssuaian_dmnsi_kndraan== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_dmnsi_kndraan" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_dmnsi_kndraan== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">D. KESESUAIAN MATERIAL</label>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_material" value="Sesuai" {{ $datakendaraan[0]->kssuaian_material== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_material" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_material== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">E. KESESUAIAN POSISI LAMPU LAMPU</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_pss_lmpu" value="Sesuai" {{ $datakendaraan[0]->kssuaian_pss_lmpu== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_pss_lmpu" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_pss_lmpu== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">F. KESESUAIAN UKURAN BAN</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_ukrn" value="Sesuai" {{ $datakendaraan[0]->kssuaian_ukrn== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_ukrn" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_ukrn== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">G. KESESUAIAN DIMENSI BAIK MUATAN</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_dmnsi_muatan" value="Sesuai" {{ $datakendaraan[0]->kssuaian_dmnsi_muatan== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_dmnsi_muatan" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_dmnsi_muatan== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">H. KESESUAIAN VOLUME BAK MUATAN</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_vlm_muatan" value="Sesuai" {{ $datakendaraan[0]->kssuaian_vlm_muatan== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_vlm_muatan" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_vlm_muatan== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">I. KESESUAIAN DIMENSI DAN JUMLAH TEMPAT DUDUK </label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_dmnsi_ddk" value="Sesuai" {{ $datakendaraan[0]->kssuaian_dmnsi_ddk== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_dmnsi_ddk" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_dmnsi_ddk== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">J. KESESUAIAN JARAK TEMPAT DUDUK</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_jrk_ddk" value="Sesuai" {{ $datakendaraan[0]->kssuaian_jrk_ddk== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_jrk_ddk" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_jrk_ddk== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
            <div class="form-group">
                {{ csrf_field() }}
                <label for="name">K. KESESUAIAN FASILITAS TEMPAT KELUAR DARUAT</label>
                <br>
                <br>
                <div class="form-group" style="margin-left: 20px">
                    <input type="radio" name="kssuaian_keluar_drrt" value="Sesuai" {{ $datakendaraan[0]->kssuaian_keluar_drrt== 'Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Sesuai</label>
                    <input type="radio" name="kssuaian_keluar_drrt" value="Tidak Sesuai" {{ $datakendaraan[0]->kssuaian_keluar_drrt== 'Tidak Sesuai' ? 'checked' : ''}} /><label style="margin-left: 10px; margin-right: 50px;">Tidak Sesuai</label>
                </div>
                <br>
            </div>
          </div>
        <button type="submit" class="btn btn-primary">Update</button>
      </form>
  </div>
</div>
<script type="text/javascript">
      function readURL1(input) {
        if (input.files && input.files[0]  ) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar1').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#img_tmpk_depan").change(function () {
        readURL1(this);
    });

    function readURL2(input) {
        if (input.files && input.files[0]  ) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar2').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#img_tmpk_smpng").change(function () {
        readURL2(this);
    });

    function readURL3(input) {
        if (input.files && input.files[0]  ) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#showgambar3').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#img_tmpk_blkng").change(function () {
        readURL3(this);
    });
    
</script>
@endsection