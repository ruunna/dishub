
<!DOCTYPE html><html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><link rel="stylesheet" type="text/css" href="cid:css-218a4cad-483e-4344-8e76-91792b221794@mhtml.blink" />


    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta name="author" content="Syamsudar Lawe Mude | Tri Cipta Internasional">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

  <title>Sistem Sertifikasi Registrasi Uji Tipe | Kementerian Perhubungan</title>

  <link rel="apple-touch-icon" sizes="57x57" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="60x60" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="72x72" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="76x76" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="114x114" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="120x120" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="144x144" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="152x152" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="apple-touch-icon" sizes="180x180" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="icon" type="image/png" sizes="192x192" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="icon" type="image/png" sizes="32x32" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="icon" type="image/png" sizes="96x96" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link rel="icon" type="image/png" sizes="16x16" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
  <link href="http://ujitiperb.dephub.go.id/front/img/logokecil.png" rel="shortcut icon">

    <link rel="manifest" href="http://ujitiperb.dephub.go.id/front/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="http://ujitiperb.dephub.go.id/front/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Arimo:300,400,500,700" rel="stylesheet">

    <!-- Bootstrap CSS File -->
    <link href="http://ujitiperb.dephub.go.id/front/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="http://ujitiperb.dephub.go.id/front/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="http://ujitiperb.dephub.go.id/front/lib/animate/animate.min.css" rel="stylesheet">
    <link href="http://ujitiperb.dephub.go.id/front/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
    <link href="http://ujitiperb.dephub.go.id/front/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="http://ujitiperb.dephub.go.id/front/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="http://ujitiperb.dephub.go.id/front/css/style.css" rel="stylesheet">
    <link href="http://ujitiperb.dephub.go.id/front/css/bs-callout.css" rel="stylesheet">

      <!-- CSRF Token -->
      <meta name="csrf-token" content="qpTMRgUx8q3NJxaRmH3bA7kqsOIHBmGqC6xCPVJa">

  <!-- Scripts -->





</head>

<body><button type="button" id="mobile-nav-toggle"><i class="fa fa-bars"></i></button>

  <header id="header" style="background:#00BCD4" class="">
      <div class="container-fluid">
        <div id="logo" class="pull-left">
          <h1><a href="http://ujitiperb.dephub.go.id/" class="scrollto"><img src="http://ujitiperb.dephub.go.id/apps/public/uploads/logo/1528776234-logo-" width="220px"></a></h1>
        </div>

        <nav id="nav-menu-container">
          <ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">
            <li class="menu-active"><a href="http://ujitiperb.dephub.go.id/#intro"><span><i class="fa fa-fw fa-home"></i></span>&nbsp;Home</a></li>
            <li><a href="http://ujitiperb.dephub.go.id/#pendaftaran"><span><i class="fa fa-fw fa-user-plus"></i></span>&nbsp;Pendaftaran</a></li>
            <li><a href="http://ujitiperb.dephub.go.id/#about"><span><i class="fa fa-fw fa-list-alt"></i></span>&nbsp;Tata Cara</a></li>
            <li><a href="http://ujitiperb.dephub.go.id/login" class="btn-get-started scrollto" role="button">Login<span>&nbsp;<i class="fa fa-fw fa-sign-in"></i></span></a></li>
          </ul>
        </nav><!-- #nav-menu-container -->
      </div>
  </header><!-- #header -->


<main id="main">
<section id="pendaftaran" class="jumbotron" style="background:#FFF !important">
  <div class="container">
<br>
<br>
<hr>
      <header class="section-header">
        <h3>DATA SRUT</h3>
      </header>

  <div class="row">
      <div class="col-md-12">
          <div class="white-box">
              <form method="POST" action="http://ujitiperb.dephub.go.id/" accept-charset="UTF-8" class="form-horizontal form-material" id="form_uji" enctype="multipart/form-data">

              <div class="row">
                  <div class="col-md-12">
                      <div class="table-responsive">
                          <hr>
                          <h5><strong style="color:#306cba !important;"> DATA KENDARAAN : </strong></h5>
                          <hr>
                                  <table class="table table-striped table-bordered">
                                      <tbody>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">No. SRUT<br>
                                                  <span class="text-success"> 140746/IX/SRUT-458/DJPD-SPD/04/2020 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">
                                                   &nbsp;
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">No. SK Rancang Bangun<br>
                                                  <span class="text-success"> KP.4366/AJ.510/DRJD/2019 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Tanggal SK Rancang Bangun<br>
                                                  <span class="text-success"> 06 - 11 - 2019 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Nomor Rangka<br>
                                                  <span class="text-success"> MHMFE74PVLK003380 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Nomor Mesin<br>
                                                  <span class="text-success"> 4D34TU21167 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info"> Merek <br>
                                                  <span class="text-success"> MITSUBISHI/COLT DIESEL FE 74 L K/(4X2) M/T </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Jenis Kendaraan<br>
                                                  <span class="text-success"> MOBIL BARANG BAK  TERTUTUP (BOX) </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Warna<br>
                                                  <span class="text-success"> KUNING </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Lampiran<br>
                                                  <div class="btn btn-sm btn-success" data-toggle="modal" data-target="#lampirankendaraan"><span class="btn-label"><i class="ti-file"></i></span>Lihat Lampiran Kendaraan</div>
                                                  <div class="modal fade" id="lampirankendaraan" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
                                                      <div class="modal-dialog  modal-lg">
                                                          <div class="modal-content">
                                                              <div class="modal-header">
                                                                  LAMPIRAN DOKUMEN KENDARAAN
                                                                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
                                                              </div>
                                                              <div class="modal-body form">
                                                                  <iframe style="width:100%; height:500px" src="cid:frame-9039E2743381CAA03262B73EE0409629@mhtml.blink"></iframe>
                                                              </div>
                                                              <div class="modal-footer">
                                                                  <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="btn-label"><i class="fa fa-close"></i></span>Keluar</button>
                                                              </div>
                                                          </div>
                                                      </div>
                                                  </div>
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                          <hr>
                          <h5><strong style="color:#306cba !important;">1. DIMENSI KENDARAAN :</strong></h5>
                          <hr>
                                  <table class="table table-striped table-bordered">
                                      <tbody>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Panjang Total<br>
                                                  <span class="text-success"> 7520 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Lebar Total<br>
                                                  <span class="text-success"> 2070 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Tinggi Total<br>
                                                  <span class="text-success"> 3200 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Jarak Sumbu 1 - 2<br>
                                                  <span class="text-success"> 4200 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Rear Overhang (ROH)<br>
                                                  <span class="text-success"> 2245 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Jarak Sumbu 2 - 3<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Front Overhang (FOH)<br>
                                                  <span class="text-success"> 1075 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Jarak Sumbu 3 - 4<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                          <hr>
                          <h5><strong style="color:#306cba !important;">2. KAROSERI KENDARAAN :</strong></h5>
                          <hr>
                                  <table class="table  table-striped table-bordered">
                                      <tbody>

                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Jumlah Silinder<br>
                                                  <span class="text-success"> 4 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Isi Silinder<br>
                                                  <span class="text-success"> 3908 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Panjang Box / Bak / Tangki<br>
                                                  <span class="text-success"> 5700 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Daya Motor<br>
                                                  <span class="text-success"> 92 KW/2900 RPM </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Lebar Box / Bak / Tangki<br>
                                                  <span class="text-success"> 2070 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Jumlah Tempat Duduk<br>
                                                  <span class="text-success"> 3 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Tinggi Box / Bak / Tangki<br>
                                                  <span class="text-success"> 2300 </span>
                                              </td>

                                              <td width="50%" class="text-uppercase text-info">Rumah Badan<br>
                                                  <span class="text-success"> 13 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">JBB/JBKB<br>
                                                  <span class="text-success"> 8000/0  </span>
                                              </td>

                                              <td width="50%" class="text-uppercase text-info">Jenis Angkutan<br>
                                                  <span class="text-success"> ANGKUTAN BARANG </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">JBI/JBKI<br>
                                                  <span class="text-success"> 8000/0 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Bahan Bakar<br>
                                                  <span class="text-success"> SOLAR </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Sudut Pergi<br>
                                                  <span class="text-success"> 15 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Pintu Keluar Darurat<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Varian<br>
                                                  <span class="text-success"> STANDAR </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Keterangan Bak/Tangki<br>
                                                  <span class="text-success">  </span>
                                              </td>

                                          </tr>
                                      </tbody>
                                  </table>
                          <hr>
                          <h5><strong style="color:#306cba !important;">3. BENTUK FISIK KENDARAAN :</strong></h5>
                          <hr>
                                              <div class="row col-md-12">
                                                  <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                                                  <img src="http://ujitiperb.dephub.go.id/apps/public/uploads/pengujian_depan/458-2020-04-14-1586846755.jpg" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
                                                   <span class="text-uppercase text-info"> Tampak Depan </span>
                                                  </div>

                                                  <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                                                  <img src="http://ujitiperb.dephub.go.id/apps/public/uploads/pengujian_samping/458-2020-04-14-1586846755.jpg" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
                                                   <span class="text-uppercase text-info"> Tampak Samping </span>
                                                  </div>

                                                  <div class="col-md-4 col-sm-4 col-xs-12 text-center">
                                                  <img src="http://ujitiperb.dephub.go.id/apps/public/uploads/pengujian_belakang/458-2020-04-14-1586846755.jpg" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
                                                  <span class="text-uppercase text-info"> Tampak Belakang </span>
                                                  </div>
                                              </div>

                          <hr>
                          <h5><strong style="color:#306cba !important;">4. KETERANGAN :</strong></h5>
                          <hr>
                                    <table class="table  table-striped table-bordered">
                                      <tbody>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 1<br>
                                                  <span class="text-success"> 7.50-16-14PR (TUNGGAL) </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 2<br>
                                                  <span class="text-success"> 7.50-16-14PR (GANDA) </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 3<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 4<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 1<br>
                                                  <span class="text-success"> 2645 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 2<br>
                                                  <span class="text-success"> 5355 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 3<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 4<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Konfigurasi Sumbu<br>
                                                  <span class="text-success"> 1.2 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Kelas Jalan<br>
                                                  <span class="text-success"> III </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Lebar Pintu Supir<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Jarak Bangku<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Berat Kosong (Kg)<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">Daya Angkut Barang (Kg)<br>
                                                  <span class="text-success"> 0 </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">Daya Angkut Penumpang (Kg)<br>
                                                  <span class="text-success"> 180 </span>
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>

                          <hr>
                          <h5><strong style="color:#306cba !important;">5. CEKLIST KESESUAIAN DATA FISIK KENDARAAN DENGAN SK RANCANG BANGUN :</strong></h5>
                          <hr>
                                    <table class="table  table-striped table-bordered">
                                      <tbody>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">a. Kesesuaian Landasan<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">g. Kesesuaian Dimensi Bak Muatan<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">b. Kesesuaian Bentuk Fisik Kendaraan<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">h. Kesesuaian Volume Bak Muatan<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">c. Kesesuaian Dimensi Kendaraan<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">i. Kesesuaian Dimensi dan Jumlah Tempat Duduk<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">d. Kesesuaian Material<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">j. Kesesuaian Jarak Tempat Duduk<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">e. Kesesuaian Posisi Lampu - Lampu<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                              <td width="50%" class="text-uppercase text-info">k. Kesesuaian Fasilitas Tempat Keluar Darurat<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                          </tr>
                                          <tr>
                                              <td width="50%" class="text-uppercase text-info">f. Kesesuaian Ukuran Ban<br>
                                                  <span class="text-success"> Sesuai </span>
                                              </td>
                                          </tr>
                                      </tbody>
                                  </table>
                      </div>
                  </div>
              </div>
              <hr>
              <div class="text-center">
              <a href="http://ujitiperb.dephub.go.id/" class="btn btn-info"><span><i class="fa fa-fw fa-home"></i></span> Home</a>
              </div>
          </form>
          </div>
      </div>
  </div>
  </div>

</section>
</main>


<footer id="footer">
  <div class="footer-top">
    <div class="container">
      <div class="row">

        <div class="col-lg-4 col-md-4 footer-info">
          <h4>Alamat</h4>
              <p class="text-justify">Jl.Medan Merdeka Barat No 8. Jakarta Pusat.  DKI Jakarta 10110 - Indonesia</p>
              <br>
              <iframe src="cid:frame-8A20D5CAF2DE3BC623ADB93CC817F368@mhtml.blink" width="100%" height="160" frameborder="0" style="border:0" allowfullscreen=""></iframe>
        </div>

        <div class="col-lg-8 col-md-8 footer-links">
          <h4>Tentang Kami</h4>
          <p class="text-justify">Untuk Peningkatan Pelayanan Penerbitan Sertifkasi Registrasi Uji Tipe Kendaraan Bermotor, Kementerian Perhubungan Mengembangkan Sistem SRUT Rancang Bangun 2018 Dimana Permohonan SRUT Dapat Diajukan Secara Online Dan Persyaratan Permohonan Sertifkasi Registrasi Uji Tipe Kendaraan Bermotor Dapat Dipelajari Pada Sistem Ini.</p>

          <div class="social-links text-left">
            <a href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/link_twitter" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a>
            <a href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/link_facebook" target="_blank" class="facebook"><i class="fa fa-facebook"></i></a>
            <a href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/link_instagram" target="_blank" class="instagram"><i class="fa fa-instagram"></i></a>
            <a href="mailto:codeigniterapps@gmail.com" target="_blank" class="google-plus"><i class="fa fa-envelope"></i></a>
          </div>
        </div>


      </div>
    </div>
  </div>
  <div class="container">
    <div class="copyright">
      © Copyright Kementerian Perhubungan RI. All Rights Reserved
    </div>
  </div>
</footer>
    <a href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/MTQwNzQ2#" class="back-to-top" style="display: none;"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->













    <!-- Contact Form JavaScript File -->


    <!-- Template Main Javascript File -->








   <div class="modal fade" id="myModalError1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header" style="display: block !important;text-align:left !important;">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
              <h3 class="modal-title">Error</h3>
          </div>
          <div class="modal-body">
              Pilih jenis pengujian terlebih dahulu.
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
          </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="myModalError2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header" style="display: block !important;text-align:left !important;">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
              <h3 class="modal-title">Error</h3>
          </div>
          <div class="modal-body">
              Pilih provinsi terlebih dahulu.
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
          </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="myModalError3" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header" style="display: block !important;text-align:left !important;">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
              <h3 class="modal-title">Error</h3>
          </div>
          <div class="modal-body">
              Data kabupaten tidak ditemukan. Gunakan yang lain.
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
          </div>
      </div>
    </div>
  </div>

   <div class="modal fade" id="myModalError4" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header" style="display: block !important;text-align:left !important;">
              <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
              <h3 class="modal-title">Error</h3>
          </div>
          <div class="modal-body">
              Data jenis kompetensi tidak ditemukan. Gunakan yang lain.
          </div>
          <div class="modal-footer">
              <button type="button" class="btn btn-danger" data-dismiss="modal">Keluar</button>
          </div>
      </div>
    </div>
  </div>








<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="><div class="lb-nav"><a class="lb-prev" href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/MTQwNzQ2"></a><a class="lb-next" href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/MTQwNzQ2"></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div><nav id="mobile-nav">
          <ul class="" style="touch-action: pan-y;" id="">
            <li class="menu-active"><a href="http://ujitiperb.dephub.go.id/#intro"><span><i class="fa fa-fw fa-home"></i></span>&nbsp;Home</a></li>
            <li><a href="http://ujitiperb.dephub.go.id/#pendaftaran"><span><i class="fa fa-fw fa-user-plus"></i></span>&nbsp;Pendaftaran</a></li>
            <li><a href="http://ujitiperb.dephub.go.id/#about"><span><i class="fa fa-fw fa-list-alt"></i></span>&nbsp;Tata Cara</a></li>
            <li><a href="http://ujitiperb.dephub.go.id/login" class="btn-get-started scrollto" role="button">Login<span>&nbsp;<i class="fa fa-fw fa-sign-in"></i></span></a></li>
          </ul>
        </nav><div id="mobile-body-overly"></div></body></html>
