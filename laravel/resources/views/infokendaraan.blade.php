
@extends('layout')
<header id="header" style="background:#00BCD4" class="">
		<div class="container-fluid">
		  <div id="logo" class="pull-left">
			<h1><a href="http://ujitiperb.dephub.go.id/" class="scrollto"><img src="http://ujitiperb.dephub.go.id/apps/public/uploads/logo/1528776234-logo-" width="220px"></a></h1> 
		  </div>

		 <!-- <nav id="nav-menu-container">-->
			<!--<ul class="nav-menu sf-js-enabled sf-arrows" style="touch-action: pan-y;">-->
			<!--  <li class="menu-active"><a href="http://ujitiperb.dephub.go.id/#intro"><span><i class="fa fa-fw fa-home"></i></span>&nbsp;Home</a></li>-->
			<!--  <li><a href="http://ujitiperb.dephub.go.id/#pendaftaran"><span><i class="fa fa-fw fa-user-plus"></i></span>&nbsp;Pendaftaran</a></li>-->
			<!--  <li><a href="http://ujitiperb.dephub.go.id/#about"><span><i class="fa fa-fw fa-list-alt"></i></span>&nbsp;Tata Cara</a></li>-->
			<!--  <li><a href="http://ujitiperb.dephub.go.id/login" class="btn-get-started scrollto" role="button">Login<span>&nbsp;<i class="fa fa-fw fa-sign-in"></i></span></a></li>-->
			<!--</ul>-->
		 <!-- </nav><!-- #nav-menu-container -->-->
		</div>
	</header>
@section('content')
    <main id="main">
 <section id="pendaftaran" class="jumbotron" style="background:#FFF !important">
    <div class="container">
<br>
<br>
<hr>
        <header class="section-header">
          <h3>DATA SRUT</h3>
        </header>

	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<form method="POST" action="http://ujitiperb.dephub.go.id/" accept-charset="UTF-8" class="form-horizontal form-material" id="form_uji" enctype="multipart/form-data">

				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<hr>
							<h5><strong style="color:#306cba !important;"> DATA KENDARAAN : </strong></h5>
							<hr>
									<table class="table table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">No. SRUT<br>
													<span class="text-success"> {{$getData[0]->no_srut}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">
													 &nbsp;
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">No. SK Rancang Bangun<br>
													<span class="text-success">{{$getData[0]->no_skrb}}</span>
												</td>
												<td width="50%" class="text-uppercase text-info">{{$getData[0]->tgl_skrb}}<br>
													<span class="text-success"> 06 - 11 - 2019 </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Nomor Rangka<br>
													<span class="text-success"> {{$getData[0]->no_rangka}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Nomor Mesin<br>
													<span class="text-success"> {{$getData[0]->no_mesin}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info"> Merek <br>
													<span class="text-success">{{$getData[0]->merek}}</span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jenis Kendaraan<br>
													<span class="text-success">{{$getData[0]->jenis_kendaraan}}</span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Warna<br>
													<span class="text-success"> {{$getData[0]->warna_kendaraan}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Lampiran<br>
													<div class="btn btn-success" data-toggle="modal" data-target="#lampirankendaraan"><span class="btn-label"><i class="ti-file"></i></span>Lihat Lampiran Kendaraan</div>
													<div class="modal fade" id="lampirankendaraan" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="static">
														<div class="modal-dialog  modal-lg">
															<div class="modal-content">
																<div class="modal-header">
																	LAMPIRAN DOKUMEN KENDARAAN
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i class="fa fa-close"></i></button>
																</div>
																<div class="modal-body form">
																	<div id="iframe_now"  style="width:100%; height:500px; border-style: inset;">
																	    <br>
																		<div class="row col-md-12" id='img_lampiran'>
                        													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
                        													<img src="{{ URL::asset($getData[0]->img_tmpk_depan) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
                        													</div>
                        													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
                        													<img src="{{ URL::asset($getData[0]->img_tmpk_smpng) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
                        													</div>
                        													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
                        													<img src="{{ URL::asset($getData[0]->img_tmpk_blkng) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
                        													</div>
                        												</div>
																	</div>
																</div>
																<div class="modal-footer">
																	<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="btn-label"><i class="fa fa-close"></i></span>Keluar</button>
																</div>
															</div>
														</div>
													</div>
												</td>
											</tr>
										</tbody>
									</table>
							<hr>
							<h5><strong style="color:#306cba !important;">1. DIMENSI KENDARAAN :</strong></h5>
							<hr>
									<table class="table table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">Panjang Total<br>
													<span class="text-success"> {{$getData[0]->panjang_ttl}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Lebar Total<br>
													<span class="text-success"> {{$getData[0]->lebar_ttl}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Tinggi Total<br>
													<span class="text-success"> {{$getData[0]->tinggi_ttl}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Sumbu 1 - 2<br>
													<span class="text-success"> {{$getData[0]->jarak_smb12}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Rear Overhang (ROH)<br>
													<span class="text-success"> {{$getData[0]->rear_overhang}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Sumbu 2 - 3<br>
													<span class="text-success"> {{$getData[0]->jarak_smb23}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Front Overhang (FOH)<br>
													<span class="text-success"> {{$getData[0]->front_overhang}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Sumbu 3 - 4<br>
													<span class="text-success"> {{$getData[0]->jarak_smb34}} </span>
												</td>
											</tr>
										</tbody>
									</table>
							<hr>
							<h5><strong style="color:#306cba !important;">2. KAROSERI KENDARAAN :</strong></h5>
							<hr>
									<table class="table  table-striped table-bordered">
										<tbody>

											<tr>
												<td width="50%" class="text-uppercase text-info">Jumlah Silinder<br>
													<span class="text-success"> {{$getData[0]->jmlh_silinder}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Isi Silinder<br>
													<span class="text-success"> {{$getData[0]->isi_silinder}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Panjang Box / Bak / Tangki<br>
													<span class="text-success"> {{$getData[0]->pjg_boxbak}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Daya Motor<br>
													<span class="text-success"> {{$getData[0]->daya_mtr}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Lebar Box / Bak / Tangki<br>
													<span class="text-success"> {{$getData[0]->lbr_boxbaktang}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jumlah Tempat Duduk<br>
													<span class="text-success"> {{$getData[0]->jmlh_tmpt_ddk}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Tinggi Box / Bak / Tangki<br>
													<span class="text-success"> {{$getData[0]->tnggi_boxbaktang}} </span>
												</td>

												<td width="50%" class="text-uppercase text-info">Rumah Badan<br>
													<span class="text-success"> {{$getData[0]->rmh_bdn}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">JBB/JBKB<br>
													<span class="text-success"> {{$getData[0]->jbb}}/{{$getData[0]->jbkb}}  </span>
												</td>

												<td width="50%" class="text-uppercase text-info">Jenis Angkutan<br>
													<span class="text-success"> {{$getData[0]->jns_angkutan}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">JBI/JBKI<br>
													<span class="text-success"> {{$getData[0]->jbi}}/{{$getData[0]->jbki}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Bahan Bakar<br>
													<span class="text-success"> {{$getData[0]->bhan_bkr}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Sudut Pergi<br>
													<span class="text-success"> {{$getData[0]->sudut_pgi}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Pintu Keluar Darurat<br>
													<span class="text-success"> {{$getData[0]->pntu_darurat}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Varian<br>
													<span class="text-success"> {{$getData[0]->varian}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Keterangan Bak/Tangki<br>
													<span class="text-success">{{$getData[0]->kt_baktang}}</span>
												</td>

											</tr>
										</tbody>
									</table>
							<hr>
							<h5><strong style="color:#306cba !important;">3. BENTUK FISIK KENDARAAN :</strong></h5>
							<hr>
												<div class="row col-md-12" id='img_lampiran'>
													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
													<img src="{{ URL::asset($getData[0]->img_tmpk_depan) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
													 <span class="text-uppercase text-info"> Tampak Depan </span>
													</div>

													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
													<img src="{{ URL::asset($getData[0]->img_tmpk_smpng) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
													 <span class="text-uppercase text-info"> Tampak Samping </span>
													</div>

													<div class="col-md-4 col-sm-4 col-xs-12 text-center">
													<img src="{{ URL::asset($getData[0]->img_tmpk_blkng) }}" style="max-height: 200px; width: 100%; min-height: 196.5px;" class="img-responsive same-artikel">
													<span class="text-uppercase text-info"> Tampak Belakang </span>
													</div>
												</div>

							<hr>
							<h5><strong style="color:#306cba !important;">4. KETERANGAN :</strong></h5>
							<hr>
									  <table class="table  table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 1<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb1}}</span>
												</td>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 2<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb2}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 3<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb3}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Ukuran Ban Pada Sumbu 4<br>
													<span class="text-success"> {{$getData[0]->ukrn_smb4}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 1<br>
													<span class="text-success"> 2645 </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 2<br>
													<span class="text-success"> 5355 </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 3<br>
													<span class="text-success"> 0 </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Kekuatan Rancangan Sumbu 4<br>
													<span class="text-success"> 0 </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Konfigurasi Sumbu<br>
													<span class="text-success"> {{$getData[0]->knfg_smb}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Kelas Jalan<br>
													<span class="text-success"> {{$getData[0]->kls_jln}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Lebar Pintu Supir<br>
													<span class="text-success"> {{$getData[0]->lbr_pntu_spr}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Jarak Bangku<br>
													<span class="text-success"> {{$getData[0]->jrk_bngku}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Berat Kosong (Kg)<br>
													<span class="text-success"> {{$getData[0]->brt_ksng}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">Daya Angkut Barang (Kg)<br>
													<span class="text-success"> {{$getData[0]->dya_angkt_brng}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">Daya Angkut Penumpang (Kg)<br>
													<span class="text-success"> {{$getData[0]->dya_angkt_pnmpng}} </span>
												</td>
											</tr>
										</tbody>
									</table>

							<hr>
							<h5><strong style="color:#306cba !important;">5. CEKLIST KESESUAIAN DATA FISIK KENDARAAN DENGAN SK RANCANG BANGUN :</strong></h5>
							<hr>
									  <table class="table  table-striped table-bordered">
										<tbody>
											<tr>
												<td width="50%" class="text-uppercase text-info">a. Kesesuaian Landasan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_lndsn}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">g. Kesesuaian Dimensi Bak Muatan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_dmnsi_muatan}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">b. Kesesuaian Bentuk Fisik Kendaraan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_bntk_fsk}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">h. Kesesuaian Volume Bak Muatan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_vlm_muatan}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">c. Kesesuaian Dimensi Kendaraan<br>
													<span class="text-success"> {{$getData[0]->kssuaian_dmnsi_kndraan}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">i. Kesesuaian Dimensi dan Jumlah Tempat Duduk<br>
													<span class="text-success"> {{$getData[0]->kssuaian_dmnsi_ddk}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">d. Kesesuaian Material<br>
													<span class="text-success"> {{$getData[0]->kssuaian_material}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">j. Kesesuaian Jarak Tempat Duduk<br>
													<span class="text-success"> {{$getData[0]->kssuaian_jrk_ddk}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">e. Kesesuaian Posisi Lampu - Lampu<br>
													<span class="text-success"> {{$getData[0]->kssuaian_pss_lmpu}} </span>
												</td>
												<td width="50%" class="text-uppercase text-info">k. Kesesuaian Fasilitas Tempat Keluar Darurat<br>
													<span class="text-success"> {{$getData[0]->kssuaian_keluar_drrt}} </span>
												</td>
											</tr>
											<tr>
												<td width="50%" class="text-uppercase text-info">f. Kesesuaian Ukuran Ban<br>
													<span class="text-success"> {{$getData[0]->kssuaian_ukrn}} </span>
												</td>
											</tr>
										</tbody>
									</table>
						</div>
					</div>
				</div>
				<hr>
				<div class="text-center">
				<a href="http://ujitiperb.dephub.go.id/" class="btn btn-info"><span><i class="fa fa-fw fa-home"></i></span> Home</a>
				</div>
			</form>
			</div>
		</div>
	</div>
	</div>
 <!-- #call-to-action -->
</section>
</main>

 <footer id="footer">
    <div class="footer-top">
      <div class="container">
        <div class="row">

          <div class="col-lg-4 col-md-4 footer-info">
            <h4>Alamat</h4>  
				<p class="text-justify">Jl.Medan Merdeka Barat No 8. Jakarta Pusat.  DKI Jakarta 10110 - Indonesia</p> 
				<br>
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3966.6569381268487!2d106.8205282148948!3d-6.176658995528272!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69f42b949bdca3%3A0xe84e3d2305bf84e9!2sJl.+Medan+Merdeka+Barat%2C+Gambir%2C+Kota+Jakarta+Pusat%2C+Daerah+Khusus+Ibukota+Jakarta+10110!5e0!3m2!1sid!2sid!4v1528592934229" width="100%" height="160" frameborder="0" style="border:0" allowfullscreen></iframe>
          </div>

          <div class="col-lg-8 col-md-8 footer-links">
            <h4>Tentang Kami</h4>
            <p class="text-justify">Untuk Peningkatan Pelayanan Penerbitan Sertifkasi Registrasi Uji Tipe Kendaraan Bermotor, Kementerian Perhubungan Mengembangkan Sistem SRUT Rancang Bangun 2018 Dimana Permohonan SRUT Dapat Diajukan Secara Online Dan Persyaratan Permohonan Sertifkasi Registrasi Uji Tipe Kendaraan Bermotor Dapat Dipelajari Pada Sistem Ini.</p>
			
            <div class="social-links text-left">
              <a href="link_twitter" target="_blank" class="twitter"><i class="fa fa-twitter"></i></a> 
              <a href="link_facebook" target="_blank"  class="facebook"><i class="fa fa-facebook"></i></a>  
              <a href="link_instagram" target="_blank"  class="instagram"><i class="fa fa-instagram"></i></a> 
              <a href="mailto:codeigniterapps@gmail.com" target="_blank"  class="google-plus"><i class="fa fa-envelope"></i></a> 
            </div>
          </div>
 

        </div>
      </div>
    </div>
    <div class="container">
      <div class="copyright">
        &copy; Copyright Kementerian Perhubungan RI. All Rights Reserved
      </div>
    </div>
  </footer>
	  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

	  <!-- JavaScript Libraries -->
	  <script src="http://ujitiperb.dephub.go.id/front/lib/jquery/jquery.min.js"></script> 
	  <script src="http://ujitiperb.dephub.go.id/front/lib/jquery/jquery-migrate.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/bootstrap/js/bootstrap.bundle.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/easing/easing.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/superfish/hoverIntent.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/superfish/superfish.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/wow/wow.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/waypoints/waypoints.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/counterup/counterup.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/owlcarousel/owl.carousel.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/isotope/isotope.pkgd.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/lightbox/js/lightbox.min.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/front/lib/touchSwipe/jquery.touchSwipe.min.js"></script>
	  <!-- Contact Form JavaScript File -->
	  <script src="http://ujitiperb.dephub.go.id/front/contactform/contactform.js"></script>

	  <!-- Template Main Javascript File -->
	  <script src="http://ujitiperb.dephub.go.id/front/js/main.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/back/js/jquery/apps.js"></script>
	  <script src="http://ujitiperb.dephub.go.id/back/js/jquery/apps-additional.js"></script> 
	  <script>  
		function toggleChevron(e) {
			$(e.target)
				.prev('.panel-heading')
				.find("i.indicator")
				.toggleClass('fa-minus fa-plus');
		}  
		$('#accordion').on('hidden.bs.collapse', toggleChevron);
		$('#accordion').on('shown.bs.collapse', toggleChevron);
	</script>  
	<script>    
	$().ready(function() {	 
		 
		$.validator.addMethod('filesize', function(value, element, param) { 
			return this.optional(element) || (element.files[0].size <= param) 
		});

		$("#form_register").validate({	 
			rules: { 
				nama: { 
					required: true,   
					minlength: 5,
					maxlength: 150,    
				}, 
				provinsi_id: { 
					required: true,     
				}, 
				bptd_id: { 
					required: true,     
				}, 
				kab_kota_id: { 
					required: true,    
				},   
				alamat: { 
					required: true,   
					minlength: 5,
					maxlength: 255,    
				}, 
				nama_pimpinan: { 
					required: true,   
					minlength: 5,
					maxlength: 50,    
				}, 
				email: { 
					required: true,   
					minlength: 5,
					maxlength: 50,     
					remote: {
						url: BASE_URL+"/email",
						type: "GET",
						data: {
						  email: function() {
							return $( "#email" ).val();
						  },  
						}
					}      
				}, 
				telepon: { 
					required: true,   
					minlength: 5,
					maxlength: 16,    
				}, 
				setuju: { 
					required: true,   
				}, 
				kode_provinsi: { 
					required: true,   
				}, 
			},
			errorElement: "span",
			messages: {   
				nama:  { 
					required: "harus diisi...",      
					minlength: "minimal 5 karakter...",   
					maxlength: "maksimal 150 karakter...",       
				},       
				bptd_id:  { 
					required: "harus diisi...",          
				},      
				provinsi_id:  { 
					required: "harus diisi...",          
				},      
				kab_kota_id:  { 
					required: "harus diisi...",          
				},             
				alamat:  { 
					required: "harus diisi...",      
					minlength: "minimal 5 karakter...",   
					maxlength: "maksimal 255 karakter...",       
				},         
				nama_pimpinan:  { 
					required: "harus diisi...",      
					minlength: "minimal 5 karakter...",   
					maxlength: "maksimal 50 karakter...",       
				},           
				email:  { 
					required: "harus diisi...",      
					minlength: "minimal 5 karakter...",   
					maxlength: "maksimal 50 karakter...",       
					remote: "email sudah digunakan. Gunakan yang lain...",       
				},               
				telepon:  { 
					required: "harus diisi...",      
					minlength: "minimal 5 karakter...",   
					maxlength: "maksimal 16 karakter...",       
				},               
				setuju:  { 
					required: "harus diisi...",       
				},              
				id_bptd:  { 
					required: "harus diisi...",       
				},            
				kode_provinsi:  { 
					required: "harus diisi...",       
				},           
			},
			submitHandler: function(form) {
			  form.submit();
			} 
		}); 
		
	}); 
	</script>
	
	 
<script> 
document.getElementById('iframe_now').onload = function() {

    // get reference to form to attach button onclick handlers
    document.getElementById('img_lampiran');
}
function updateIframe(){
        var myFrame = $("#iframe_now").contents().find('body');
        var textareaValue = document.getElementById('img_lampiran');
        myFrame.html(textareaValue);
    }
$(document).ready(function() {

    $('select[name="bptd_id"]').on('change', function(){
        var id_bptd = $(this).val();
        if(id_bptd) {
            $.ajax({
				url: BASE_URL+"/provinsi/"+id_bptd, 
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="provinsi_id"]').empty();

                    $.each(data, function(key, value){
                            $('select[name="provinsi_id"]').append('<option value="'+ key +'">'+ value +'</option>'); 

                    });
						$('select[name="provinsi_id"]').trigger( "change" );
                },
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="provinsi_id"]').empty();
        }

    });

    $('select[name="provinsi_id"]').on('change', function(){
        var provinsi_id = $(this).val();
        if(provinsi_id) {
            $.ajax({
				url: BASE_URL+"/kabupaten/"+provinsi_id, 
                type:"GET",
                dataType:"json",
                beforeSend: function(){
                    $('#loader').css("visibility", "visible");
                },

                success:function(data) {

                    $('select[name="kab_kota_id"]').empty();

                    $.each(data, function(key, value){
                            $('select[name="kab_kota_id"]').append('<option value="'+ key +'">'+ value +'</option>'); 

                    });
                },
					error: function (jqXHR, textStatus, errorThrown)
					{
                        $('select[name="kab_kota_id"]').empty(); 
                        $('select[name="provinsi_id"]').empty(); 
						$('#myModalError3').modal('show');
					},
                complete: function(){
                    $('#loader').css("visibility", "hidden");
                }
            });
        } else {
            $('select[name="kab_kota_id"]').empty();
                        $('select[name="provinsi_id"]').empty(); 
				$('#myModalError2').modal('show');
        }

    });
		
		$('select[name="provinsi_id"]').on('change', function() {
			var provinsi_id = $(this).val();
			if(provinsi_id) {
				$.ajax({
					url: BASE_URL+"/formkodeprovinsi/"+provinsi_id,
					type: "GET",
					dataType: "json",
					success:function(data) {
						 $('input[name="kode_provinsi"]').val(data.kode_provinsi);  
					}
				});
			}else{ 
				$('input[name="kode_provinsi"]').val("");
			}
		});

}); 
</script> 
	
@endsection


