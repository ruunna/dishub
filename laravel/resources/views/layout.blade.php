<!DOCTYPE html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8"><link rel="stylesheet" type="text/css" href="cid:css-218a4cad-483e-4344-8e76-91792b221794@mhtml.blink" />
<head>
    <meta charset="utf-8" />
    <link rel="icon" type="image/png" href="assets/img/favicon.ico">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />        
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">
    <meta name="author" content="Syamsudar Lawe Mude | Tri Cipta Internasional">
    <meta name="robots" content="index follow">
    <meta name="googlebot" content="index follow">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width" />


	<title>Sistem Sertifikasi Registrasi Uji Tipe | Kementerian Perhubungan</title> 
	
	<link rel="apple-touch-icon" sizes="57x57" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="60x60" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="72x72" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="76x76" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="114x114" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="120x120" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="144x144" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="152x152" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="apple-touch-icon" sizes="180x180" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="icon" type="image/png" sizes="192x192" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="icon" type="image/png" sizes="32x32" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="icon" type="image/png" sizes="96x96" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link rel="icon" type="image/png" sizes="16x16" href="http://ujitiperb.dephub.go.id/front/img/logokecil.png">
	<link href="http://ujitiperb.dephub.go.id/front/img/logokecil.png" rel="shortcut icon">
	
	  <link rel="manifest" href="http://ujitiperb.dephub.go.id/front/favicon/manifest.json">
	  <meta name="msapplication-TileColor" content="#ffffff">
	  <meta name="msapplication-TileImage" content="http://ujitiperb.dephub.go.id/front/favicon/ms-icon-144x144.png">
	  <meta name="theme-color" content="#ffffff">

	  <!-- Google Fonts -->
	  <link href="https://fonts.googleapis.com/css?family=Arimo:300,400,500,700" rel="stylesheet">

	  <!-- Bootstrap CSS File -->
	  <link href="http://ujitiperb.dephub.go.id/front/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	  <!-- Libraries CSS Files -->
	  <link href="http://ujitiperb.dephub.go.id/front/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
	  <link href="http://ujitiperb.dephub.go.id/front/lib/animate/animate.min.css" rel="stylesheet">
	  <link href="http://ujitiperb.dephub.go.id/front/lib/ionicons/css/ionicons.min.css" rel="stylesheet">
	  <link href="http://ujitiperb.dephub.go.id/front/lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
	  <link href="http://ujitiperb.dephub.go.id/front/lib/lightbox/css/lightbox.min.css" rel="stylesheet">

	  <!-- Main Stylesheet File -->
	  <link href="http://ujitiperb.dephub.go.id/front/css/style.css" rel="stylesheet">
	  <link href="http://ujitiperb.dephub.go.id/front/css/bs-callout.css" rel="stylesheet"> 
    <!-- Bootstrap core CSS     -->
    <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{ asset('assets/css/animate.min.css') }}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{ asset('assets/css/light-bootstrap-dashboard.css?v=1.4.0') }}" rel="stylesheet"/>


    <!--  CSS for Demo Purpose, don't include it in your project     -->
    <link href="{{ asset('assets/css/demo.css') }}" rel="stylesheet" />


    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'>
    <link href="{{ asset('assets/css/pe-icon-7-stroke.css') }}" rel="stylesheet" />
    <link rel="stylesheet" href="{{ asset('css/css.css') }}">


    <!--   Core JS Files   -->
    <script src="{{ asset('assets/js/jquery.3.2.1.min.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.19/js/dataTables.material.min.js"></script>
    <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
    <style type="text/css">
  
      .modal-header { display:block !important; }
	.error { color:#E91E63; }
	body { font-family:Arimo; font-size:14px !important; }
    </style>
</head>
<body>
        
        @yield('content')
    
</body>
<div id="lightboxOverlay" class="lightboxOverlay" style="display: none;"></div><div id="lightbox" class="lightbox" style="display: none;"><div class="lb-outerContainer"><div class="lb-container"><img class="lb-image" src="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="><div class="lb-nav"><a class="lb-prev" href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/MTQwNzQ2"></a><a class="lb-next" href="http://ujitiperb.dephub.go.id/cekdata/NDc5MTg=/MTQwNzQ2"></a></div><div class="lb-loader"><a class="lb-cancel"></a></div></div></div><div class="lb-dataContainer"><div class="lb-data"><div class="lb-details"><span class="lb-caption"></span><span class="lb-number"></span></div><div class="lb-closeContainer"><a class="lb-close"></a></div></div></div></div><nav id="mobile-nav">
			<ul class="" style="touch-action: pan-y;" id="">
			  <li class="menu-active"><a href="http://ujitiperb.dephub.go.id/#intro"><span><i class="fa fa-fw fa-home"></i></span>&nbsp;Home</a></li>
			  <li><a href="http://ujitiperb.dephub.go.id/#pendaftaran"><span><i class="fa fa-fw fa-user-plus"></i></span>&nbsp;Pendaftaran</a></li>
			  <li><a href="http://ujitiperb.dephub.go.id/#about"><span><i class="fa fa-fw fa-list-alt"></i></span>&nbsp;Tata Cara</a></li>
			  <li><a href="http://ujitiperb.dephub.go.id/login" class="btn-get-started scrollto" role="button">Login<span>&nbsp;<i class="fa fa-fw fa-sign-in"></i></span></a></li>
			</ul>
		  </nav><div id="mobile-body-overly"></div>
</html>