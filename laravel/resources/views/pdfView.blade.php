<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Halaman Print A4</title> 
</head>
<style type="text/css">
  @media print {
      html, body {
          display: block;
          font-size: 12pt; 
          margin-left:8px;
          margin-right:20px;
          font-family: Arial;
          /*font-weight: bold;*/
          /*line-height:0.5;*/
          /*font-style:bold;*/
      }

      @page {
        /* size: 11.00cm 8.5cm; */
        size: 24.20cm 30.5cm;
        /* 10.16cm 15.2cm */
        /* 21,0 x 29,7 cm */
        
      }

      .logo {
        width: 30%;
      }
  }
  .column {
        float: left;
        width: 50%;
      }

      /* Clear floats after the columns */
      .row:after {
        content: "";
        display: table;
        clear: both;
      }
  img {
    position: absolute;
    right: 70px;
    top: 110px;
    z-index: 2;
  }
  div.coba{
     /*border-style:ridge; */
     /*border-width:1px; */
  }
  table, th, td{
    line-height:0.5 ;
     border: 1px solid transparent;
  }
  tr{
    margin-left:5px;
    margin-right: 5px;
    height: 25px;
    margin-bottom:1px;
    line-height:0.5 ;
     /*border: 2px ridge ; */
  }
 textarea{  
  /* box-sizing: padding-box; */
  overflow:hidden;
  /* demo only: */
  padding:10px;
  width:500px;
  margin-left:10px auto;
  border: none;
  resize: none;
  overflow: auto;    
  font-size: 12pt;
  font-weight: bold;
  font-family: Arial;
}
div{
    font-size: 12pt;
    font-weight: bold;
    font-family: Arial;
}
  
  
</style>

<body onload="window.print(); ">
  <div>
    <div style='margin-top:35px; '>
        <p style='text-align:left; margin-left:285px;font-family: calibri;   font-weight: bold; font-size: 14pt;'>
            <b>{{$data[0]->no_srut}}</b>
        </p>
    </div>
    <div style='margin-top:0px;'>
     <div style='margin-left:150px; display: inline-block; '>
        <textarea rows='1' style='margin: 0px; width: 200px; height: 29px;'>{{$data[0]->perusahaan_pembuat}}</textarea></div>
        <div style='margin-left:45px; display: inline-block; position:relative; z-index: 1;'>
        <textarea rows='1' style='margin: 0px; width: 300px; height: 32px;
     '>{{$data[0]->alamat_pembuat}}</textarea>
      </div>
    </div>
    <div style='margin-top:0px; line-height:0.5; margin-left:295px;'>
        <p style='line-height:0.25; '>{{$data[0]->jenis_kendaraan}}</p>
        <p style='line-height:0.25; '>{{$data[0]->merek}}</p>
        <p style='line-height:0.25; '>{{$data[0]->jenis_kendaraan}}</p>
        <p style='line-height:0.25; '>{{$data[0]->jns_angkutan}}</p>
        <p style='line-height:0.25; '>{{$data[0]->varian}}</p>
        <p style='align-text:left center;'>..</p>
        <p style='line-height:0.25; '>{{$data[0]->no_rangka}}</p>
        <p style='line-height:0.25; '>{{$data[0]->no_mesin}}</p>
        <p style='line-height:0.25; '>{{$data[0]->penanggungJwb_pembuat}}</p>
    </div>
    <div class='coba'>
        <img style="width:118px; height:118px;" src="images/{{str_replace("/","-",$data[0]->qrCode)}}"/>
    </div>
    <div class='coba'>
    </div>
    <div style='margin-top:88px; text-align:center; '>
    <table width='100%' style='margin-left:60px; margin-top:121px; text-align:left; padding-left:20px;padding-right:70px;margin-right:40px;' cellspacing="10">
       <tr>
        <th width="100px" hidden>(width=100px)</th>
        <th width="50px"hidden>(width=50px)</th>
        <th width="300px"hidden>(width=300px)</th>
        <th width="50px"hidden>(width=50px)</th>
        <th width="50px"hidden>(width=50px)</th>
        <th width="50px"hidden>(width=50px)</th>
        <th width="300px"hidden>(width=300px)</th>
        <th width="100px"hidden>(width=100px)</th>
      </tr>
      <tr>
          <td style='line-height:0.5 ; text-align: left; '>{{$data[0]->knfg_smb}}</td>
          <td style='line-height:0.5 ;  text-align: left; '>{{$data[0]->jarak_smb12}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->lebar_ttl}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->jmlh_silinder}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->daya_mtr}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->bhan_bkr}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->ukrn_smb1}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->kekuatan_rancangan_smb1}}</td>
      </tr>
      <tr>
          <td style='line-height:0.5 ; text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left; font-size: 9pt;'>{{$data[0]->jarak_smb23}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->panjang_ttl}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->isi_silinder}}</td>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->ukrn_smb2}}</td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->kekuatan_rancangan_smb2}}</td>
      </tr>
      <tr>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'>{{$data[0]->jarak_smb34}}</td>
          <td style='line-height:0.5 ;  text-align: left;'> {{$data[0]->tinggi_ttl}}</td>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->ukrn_smb3}}</td>
          <td style='line-height:0.5 ;   text-align: left;'>{{$data[0]->kekuatan_rancangan_smb3}}</td>   
      </tr>
      <tr>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->rear_overhang}}</td>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->ukrn_smb4}}</td>
          <td style='line-height:0.5 ;   text-align: left;'>{{$data[0]->kekuatan_rancangan_smb4}}</td>
      </tr>
      <tr>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'>{{$data[0]->front_overhang}}</td>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
          <td style='line-height:0.5 ;  text-align: left;'></td>
          <td style='line-height:0.5 ;   text-align: left;'></td>
      </tr>
      </table>
    </div>
    <div class='coba' style='margin-top:-12px; margin-left:65px;  '>
      <p style='line-height:0.5;'>
        <span style='margin-left:104px;  '>{{$data[0]->jbb}} </span>
        <span style='margin-left:73px;  '>{{$data[0]->jbkb}} </span>
      </p>
      <p style='line-height:0.5;'>
        <span style='margin-left:104px;  '>{{$data[0]->brt_ksng}} </span>
      </p>
      <p style='line-height:0.5;'>
        <span style='margin-left:104px;  '>{{$data[0]->jbi}}</span>
        <span style='margin-left:73px;  '>{{$data[0]->jbki}}</span>
      </p>
      <p style='line-height:0.5;'>
        <span style='margin-left:104px;  '>{{$data[0]->dya_angkt_pnmpng}}</span>
        <span style='margin-left:143px;  '>{{$data[0]->dya_angkt_brng}}</span>
        <span style='margin-left:273px;  '></span>
      </p>
      <p style='margin-top:35px'>
        <span style='margin-left:224px; line-height:0.25; '>{{$data[0]->tnggi_box_bak_tang}}</span>
      </p>
      <br>
      <p style='margin-top:-3px;'>
        <span style='margin-left:330px; line-height:0.25; '>{{$data[0]->kls_jln}} </span>
      </p>
    </div>
    <div style='margin-top:28px;' class='coba'>
      <p>
          <span style='margin-left:253px; '>KEPUTUSAN DIREKTUR JENDRAL PERHUBUNGAN DARAT NOMOR</span>
          <br>
          <span style='margin-left:253px; '>{{$data[0]->no_skrb}} TANGGAL {{$data[0]->tgl_skrb}}</span>
        </p>
    </div>
    <div style='margin-top:27px; margin-right:200px; text-align: right;' class=''>
      <p>
          <span>Jakarta {{ app('request')->input('tanggalPengesah') }} </span>
      </p>
    </div>
    <div style='margin-top:25px; text-align: left;' class=''>
      <p>
          <span style='margin-left:39px; '>DIREKTUR SARANA PERHUBUNGAN DARAT</span>
      </p>
    </div>
    <div class=''>
      <div class="row" style='margin-top:42px;'>
        <div class="column" style='margin-top:30px;'>
          <span style='margin-left:60px; '>{{ app('request')->input('dirutSarana') }} </span>
          <span style='margin-left:70px; '>PANDU YUNIANTO</span>
          <br>
          <span style='margin-left:70px; '>{{ app('request')->input('NIPdirutSarana') }}</span>
                    <span style='margin-left:57px; '>3650606 198803 1 001</span>
        </div>
        <div class="column" style='margin-top:12px;'>
          <span style='margin-left:65px; '>{{ app('request')->input('pengesah') }} </span>
          <br>
          <span style='margin-left:65px; '>{{ app('request')->input('jabatanPengesah') }}</span>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
 <script type="text/javascript">window.print();
 var textarea = document.querySelector('textarea');

textarea.addEventListener('keydown', autosize);
             
function autosize(){
  var el = this;
  setTimeout(function(){
    el.style.cssText = 'height:auto; padding:0';
    // for box-sizing other than "content-box" use:
    // el.style.cssText = '-moz-box-sizing:content-box';
    el.style.cssText = 'height:' + el.scrollHeight + 'px';
  },0);
}</script> 