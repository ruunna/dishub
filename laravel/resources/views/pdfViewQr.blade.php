<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Halaman Print A4</title> 
</head>
<style type="text/css">
  @media print {
      html, body {
          display: block;
          font-size: 10px; 
          /* margin-left:20px;
          margin-right:20px; */
          font-family:arial;
          font-size: 12px;  
      }

      @page {
        /* size: 11.00cm 8.5cm; */
        size: 24.20cm 30.5cm;
        /* 10.16cm 15.2cm */
        /* 21,0 x 29,7 cm */
        
      }

      .logo {
        width: 30%;
      }
  }
  .column {
        float: left;
        width: 50%;
      }

      /* Clear floats after the columns */
      .row:after {
        content: "";
        display: table;
        clear: both;
      }
  img {
    position: absolute;
    right: 75px;
    top: 165px;
    z-index: -1;
  }
  div.coba{
    /* border-style:ridge; */
    /* border-width:1px; */
  }

  .users {
    border-style: solid;
  table-layout: fixed;
  width:1000px;
  height:200px;
  /* height: 50px; */
  /* line-height: 1.6; */
  /* white-space: nowrap; */
  white-space: normal;
  word-break: break-all;
}
/* Column widths are based on these cells */

.users tr{
  width:50px;
  height:50px;
}
.users td {
  width:50px;
  height:50px;
  overflow:hidden;
  word-wrap:break-word; 
  border-style: solid;
  /* white-space: nowrap; */
  overflow: hidden;
  overflow-wrap: break-word;
  /* text-overflow: ellipsis; */
  word-break: break-all;
}
.users th {
  border-style: solid;
  background: darkblue;
  color: white;
  word-break: break-all;
}
/* .users td,
.users th {
  border-style: solid;
  text-align: left;
  width:50px;
  height:50px;;
  padding: 5px 10px;
  overflow: hidden;
  word-break: break-all;
} */
.users tr:nth-child(even) {
  background: lightblue;
}
</style>

<!-- <body onload="window.print(); window.close();"> -->
<body>
  <div>
    <div style='margin-top:95px;'>
      <p style='text-align:center; font-style: blod; font-size: 12px;'><span>{{$data[0]->no_srut}}</span></p>
    </div>
    <br>
    <div>
      <p>
        <span style='margin-left:110px; font-size: 12px;'>{{$data[0]->perusahaan_pembuat}}</span>
        <span style='margin-left:70px; font-size: 12px;'>{{$data[0]->alamat_pembuat}}</span>
      </p>
    </div>
    <div style='margin-top:30px; line-height:0.5;'>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->jenis_kendaraan}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->merek}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->jenis_kendaraan}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->jns_angkutan}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->varian}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>..</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->no_rangka}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->no_mesin}}</p>
        <p style='margin-left:220px; line-height:0.5; font-size: 12px;'>{{$data[0]->penanggungJwb_pembuat}}</p>
    </div>
    <div class='coba'>
        <img style="width:100px; height:100px;" src="images/{{str_replace("/","-",$data[0]->qrCode)}}"  class="img-thumbnail"/>
    </div>
    <div class='coba'>
    </div>
    <div style='margin-top:140px; text-align:center; '>
    <!-- <table width='100%' style='margin-top:120px; text-align:center; padding-left:10px;padding-right:30px;'>
      <tr>
          <td>{{$data[0]->knfg_smb}}</td>
          <td>{{$data[0]->jarak_smb12}}</td>
          <td>{{$data[0]->lebar_ttl}}</td>
          <td>{{$data[0]->jmlh_silinder}}</td>
          <td>{{$data[0]->daya_mtr}}</td>
          <td>{{$data[0]->bhan_bkr}}</td>
          <td>{{$data[0]->ukrn_smb1}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb1}}</td>
      </tr>
      <tr>
          <td></th>
          <td>{{$data[0]->jarak_smb23}}</td>
          <td>2) {{$data[0]->panjang_ttl}}</td>
          <td>{{$data[0]->isi_silinder}}</td>
          <td></td>
          <td></td>
          <td>{{$data[0]->ukrn_smb2}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb2}}</td>
      </tr>
      <tr>
          <td></td>
          <td>{{$data[0]->jarak_smb34}}</td>
          <td>{{$data[0]->tinggi_ttl}}</td>
          <td></td>
          <td></td>
          <td'></td>
          <td>{{$data[0]->ukrn_smb3}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb3}}</td>   
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td>{{$data[0]->rear_overhang}}</td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{$data[0]->ukrn_smb4}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb4}}</td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td>{{$data[0]->front_overhang}}</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
      </table> -->

    <!-- testing -->
    <table class="users">
  <tbody>
  <tr>
          <td>{{$data[0]->knfg_smb}}</td>
          <td>{{$data[0]->jarak_smb12}}</td>
          <td>{{$data[0]->lebar_ttl}}</td>
          <td>{{$data[0]->jmlh_silinder}} asdasdasd asdasdasd asdasdasd asdasdasd asdasdasdas asdasdasd</td>
          <td>{{$data[0]->daya_mtr}}</td>
          <td>{{$data[0]->bhan_bkr}}</td>
          <td>{{$data[0]->ukrn_smb1}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb1}}</td>
      </tr>
      <tr>
          <td></th>
          <td>{{$data[0]->jarak_smb23}}</td>
          <td>{{$data[0]->panjang_ttl}}</td>
          <td>{{$data[0]->isi_silinder}}</td>
          <td></td>
          <td></td>
          <td>{{$data[0]->ukrn_smb2}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb2}}</td>
      </tr>
      <tr>
          <td></td>
          <td>{{$data[0]->jarak_smb34}}</td>
          <td>{{$data[0]->tinggi_ttl}}</td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{$data[0]->ukrn_smb3}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb3}}</td>   
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td>{{$data[0]->rear_overhang}} </td>
          <td></td>
          <td></td>
          <td></td>
          <td>{{$data[0]->ukrn_smb4}}</td>
          <td>{{$data[0]->kekuatan_rancangan_smb4}}</td>
      </tr>
      <tr>
          <td></td>
          <td></td>
          <td>{{$data[0]->front_overhang}}</td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
          <td></td>
      </tr>
  </tbody>
</table>
    <!-- testing -->
    </div>
    <div class='coba' style='margin-top:38px;'>
      <p style='line-height:0.5;'>
        <span style='margin-left:120px;  font-size: 12px;'>{{$data[0]->jbb}} kg</span>
        <span style='margin-left:40px;  font-size: 12px;'>{{$data[0]->jbkb}} kg</span>
      </p>
      <p style='line-height:0.5;'>
        <span style='margin-left:120px;  font-size: 12px;'>{{$data[0]->brt_ksng}} kg</span>
      </p>
      <p style='line-height:0.5;'>
        <span style='margin-left:120px;  font-size: 12px;'>{{$data[0]->jbi}} kg</span>
        <span style='margin-left:40px;  font-size: 12px;'>{{$data[0]->jbki}} kg</span>
      </p>
      <p style='line-height:0.5;'>
        <span style='margin-left:120px;  font-size: 12px;'>{{$data[0]->dya_angkt_pnmpng}} kg</span>
        <span style='margin-left:100px;  font-size: 12px;'>{{$data[0]->dya_angkt_brng}} kg</span>
        <span style='margin-left:250px;  font-size: 12px;'></span>
      </p>
      <p style='margin-top:25px'>
        <span style='margin-left:200px; line-height:0.25; font-size: 12px;'>{{$data[0]->tnggi_box_bak_tang}}</span>
      </p>
      <br>
      <p style='margin-top:5px;'>
        <span style='margin-left:300px; line-height:0.25; font-size: 12px;'>{{$data[0]->kls_jln}} <span>
      </p>
    </div>
    <div style='margin-top:32px;' class='coba'>
      <p>
          <span style='margin-left:200px; font-size: 12px;'>KEPUTUSAN DIREKTUR JENDRAL PERHUBUNGAN DARAT NOMOR</span>
          <br>
          <span style='margin-left:200px; font-size: 12px;'>{{$data[0]->no_skrb}} TANGGAL {{$data[0]->tgl_skrb}}</span>
        </p>
    </div>
    <div style='margin-top:10px; margin-right:100px; text-align: right;' class=''>
      <p>
          <span>JAKARTA {{ app('request')->input('tanggalPengesah') }} </span>
      </p>
    </div>
    <div style='margin-top:35px; text-align: left;' class=''>
      <p>
          <span style='margin-left:40px; font-size: 12px;'>DIREKTUR SARANA PERHUBUNGAN DARAT </span>
      </p>
    </div>
    <div class=''>
      <div class="row" style='margin-top:65px;'>
        <div class="column">
          <span style='margin-left:60px; font-size: 12px;'>{{ app('request')->input('dirutSarana') }} </span>
          <br>
          <span style='margin-left:80px; font-size: 12px;'>{{ app('request')->input('NIPdirutSarana') }}</span>
        </div>
        <div class="column">
          <span style='margin-left:80px; font-size: 12px;'>{{ app('request')->input('pengesah') }} </span>
          <br>
          <span style='margin-left:80px; font-size: 12px;'>{{ app('request')->input('jabatanPengesah') }}</span>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
<!-- <script type="text/javascript">window.print();</script> -->