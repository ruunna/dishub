<?php


Route::resource('shares', 'ShareController');
Route::get('/', 'ShareController@index')->name('dashboard');
Route::get('/home', 'ShareController@home')->name('home');
Route::get('/shares/create', 'ShareController@create');
Route::get('/shares/datakendaraan/idkendaraan={idkendaraan}', 'DataKendaraanController@getData');
Route::get('/blank', 'DataKendaraanController@blankView');

Route::get('/detail/idkendaraan={idkendaraan}', 'DataKendaraanController@detailkendaraan')->name('detaildata');;
Route::get('/datakendaraan/list/{idkendaraan}', 'DataKendaraanController@getDataJson');
Route::get('/detail', 'DataKendaraanController@printPDF')->name('printPDF');

Route::get('/login', 'UserController@login');
Route::post('/loginPost', 'UserController@loginPost');
Route::get('/logout', 'UserController@logout');
Route::get('/register', 'UserController@register');
Route::post('/registerPost', 'UserController@registerPost');


// Auth::routes();
// Route::get('/home', 'HomeController@index')->name('home');
