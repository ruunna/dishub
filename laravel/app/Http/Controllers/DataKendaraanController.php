<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use DB;
use PDF;
use Session;

class DataKendaraanController extends Controller
{

    public function getData($idkendaraan)
    {
        // 464568/-/SRUT-516/DJPD.SPD/04/2019
        // view dari qr
        // dd($idkendaraan);
        // $decrypt = Crypt::decryptString($idkendaraan);
        $getData = DB::table('datakendaraans')
                    ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
                    ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
                    ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
                    ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
                    ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
                    ->where('datakendaraans.idkendaraan','=', $idkendaraan)
                    ->get();
                    // dd(count($getData));
        $data=count($getData);
        if($data==0){
            return view('notfound');
        }else{
            return view('infokendaraan',['getData' => $getData] );
        }
    }

    public function getDataJson($idkendaraan)
    {

        $getData = DB::table('datakendaraans')
        ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
        ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
        ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
        ->where('datakendaraans.idkendaraan','=', $idkendaraan)
        ->get();
        return response()->json($getData);
    }

    public function blankView()
    {
        return redirect('blank');
    }

    public function detailkendaraan($idkendaraan)
    {
        // $idkendaraan=$request->idkendaraan;


        $getData = DB::table('datakendaraans')
                    ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
                    ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
                    ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
                    ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
                    ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
                    ->where('datakendaraans.idkendaraan','=', $idkendaraan)
                    ->get();
        if($getData!= '' || $getData!= null){

//             $image = QrCode::format('png')->merge('https://ujitipeditsrana.id/shares/datakendaraan/idkendaraan='.$getData[0]->idkendaraan, 0.3, true)
//                 ->size(200)->errorCorrection('H')
//                 ->generate('W3Adda Laravel Tutorial');
// return response($image)->header('Content-type','image/png');
            return view('shares.detailkendaraan',['getData' => $getData]);
        }else{
            return view('notfound');
        }

    }

    public function printPDF(Request $request)
    {
        $idKendaraan = $request->idKendaraan;
        $dirutSarana = $request->dirutSarana;
        $NIPdirutSarana =$request->NIPdirutSarana;
        $pengesah = $request->pengesah;
        $jabatanPengesah = $request->jabatanPengesah;

        $tanggalPengesah = $request->tanggalPengesah;

        // $print=1;
        $getData = DB::table('datakendaraans')
        ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
        ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
        ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
        ->where('datakendaraans.idkendaraan','=', $idKendaraan)
        ->get();
        return view('pdfView',['data'=>$getData]);
        // return view('pdfView')->with('data', $getData->getData()->Data);
    }

}
