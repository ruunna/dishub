<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Validator;
use App\Share;
use App\Datakendaraan;
use App\Dimensi;
use App\Karoseri;
use File;
use DB;
use Datatables;
use Log;
use Qr;

class ShareController extends Controller
{

    /**
     * Validate the user insert request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateInsert(Request $request)
    {
        $request->validate([

            'no_srut'=> 'required|min:5',
            'no_skrb'=> 'required|min:5',
            'tgl_skrb'=> 'required|string',
            'no_rangka'=> 'required|string',
            'no_mesin'=> 'required|string',
            'merek'=> 'required|string',
            'jenis_kendaraan'=> 'required|string',
            'warna_kendaraan'=> 'required|string',
            'lampiran'=> 'required|string',
            'jmlh_silinder'=> 'required|string',
            'isi_silinder'=> 'required|string',
            'pjg_boxbak'=> 'required|string',
            'tnggi_boxbaktang'=> 'required|string',
            'jmlh_tmpt_ddk'=> 'required|string',
            'daya_mtr'=> 'required|string',
            'lbr_boxbaktang'=> 'required|string',
            'rmh_bdn'=> 'required|string',
            'jbb'=> 'required|string',
            'jbkb'=> 'required|string',
            'jns_angkutan'=> 'required|string',
            'jbi'=> 'required|string',
            'jbki'=> 'required|string',
            'bhan_bkr'=> 'required|string',
            'sudut_pgi'=> 'required|string',
            'pntu_darurat'=> 'required|string',
            'varian'=> 'required|string',
            'kt_baktang'=> 'required|string',
            'panjang_ttl'=> 'required|string',
            'lebar_ttl'=> 'required|string',
            'tinggi_ttl'=> 'required|string',
            'jarak_smb12'=> 'required|string',
            'rear_overhang'=> 'required|string',
            'jarak_smb23'=>  'required|string',
            'front_overhang'=> 'required|string',
            'jarak_smb34'=>  'required|string',
            'img_tmpk_depan'=> 'image|mimes:jpg,png,jpeg|max:5000',//4mb
            'img_tmpk_smpng'=> 'image|mimes:jpg,png,jpeg|max:5000',//4mb
            'img_tmpk_blkng'=> 'image|mimes:jpg,png,jpeg|max:5000',//4mb
            'ukrn_smb1' => 'required|string',
            'ukrn_smb2' => 'required|string',
            'ukrn_smb3' => 'required|string',
            'ukrn_smb4' => 'required|string',
            'kekuatan_rancangan_smb1' => 'required|string',
            'kekuatan_rancangan_smb2' => 'required|string',
            'kekuatan_rancangan_smb3' => 'required|string',
            'kekuatan_rancangan_smb4' => 'required|string',
            'tnggi_box_bak_tang' => 'required|string',
            'rmh_bdn_2' => 'required|string',
            'knfg_smb' => 'required|string',
            'kls_jln' => 'required|string',
            'lbr_pntu_spr' => 'required|string',
            'jrk_bngku' => 'required|string',
            'brt_ksng' => 'required|string',
            'dya_angkt_brng' => 'required|string',
            'dya_angkt_pnmpng' => 'required|string',
            'kssuaian_lndsn' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_bntk_fsk' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_dmnsi_kndraan' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_material' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_pss_lmpu' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_ukrn' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_dmnsi_muatan' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_vlm_muatan' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_dmnsi_ddk' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_jrk_ddk' => 'required|in:Sesuai,Tidak Sesuai',
            'kssuaian_keluar_drrt' =>'required|in:Sesuai,Tidak Sesuai'
        ],[
            'no_srut.required' => ' Nomor Srut harus diisi.',
            'no_srut.min' => ' Nomor Srut minimal 5 karakter.',
            'no_srut.max' => ' Nomor Srut maksimal 35 karakter',
            'no_skrb.required' => ' Nomor SK Rancang Bangun harus diisi.',
            'no_skrb.min' => ' Nomor SK Rancang Bangun minimal 5 karakter.',
            'no_skrb.max' => ' Nomor SK Rancang Bangun tidak melebihi 35 karakter.',
            'numeric' => ' Isi inputan harus berupa angka',
            'tgl_skrb.required'=> 'Tanggal SK Rancang Bangun harus diisi.',
            'no_rangka.required'=> ' Nomor Rangka harus diisi.',
            'no_mesin.required'=> 'Nomor Mesinharus diisi.',
            'merek.required'=> 'Merek harus diisi.',
            'jenis_kendaraan.required'=> 'Jenis kendaraan harus diisi.',
            'warna_kendaraan.required'=> 'Warna kendaraan harus diisi.',
            'lampiran.required'=> 'Lampiran harus diisi.',
            'jmlh_silinder.required'=> 'Jumlah silinder harus diisi.',
            'isi_silinder.required'=> 'Isi silinder harus diisi.',
            'pjg_boxbak.required'=> 'Panjang Box/Bak harus diisi.',
            'tnggi_boxbaktang.required'=> 'Tinggi Box/Bak harus diisi.',
            'jmlh_tmpt_ddk.required'=> 'Jumlah tempat dudukharus diisi.',
            'daya_mtr.required'=> 'Daya Motor harus diisi.',
            'lbr_boxbaktang.required'=> 'Lebar Box/Bak/Tang harus diisi.',
            'rmh_bdn.required'=> 'Rumah Badan harus diisi.',
            'jbb.required'=> 'JBB harus diisi.',
            'jbkb.required'=> 'JBKB harus diisi.',
            'jns_angkutan.required'=> 'Jenis angkutan harus diisi.',
            'jbi.required'=> 'JBI harus diisi.',
            'jbki.required'=> 'JBKI harus diisi.',
            'bhan_bkr.required'=> 'Bahan Bakar harus diisi.',
            'sudut_pgi.required'=> 'Sudut perigi harus diisi.',
            'pntu_darurat.required'=> 'Pintu darurat harus diisi.',
            'varian.required'=> 'Varian harus diisi.',
            'kt_baktang'=> 'Keterangan Bak/Tang harus diisi.',
            'panjang_ttl.required'=> 'Panjang total harus diisi.',
            'lebar_ttl.required'=> 'Lebar total harus diisi.',
            'tinggi_ttl.required'=> 'Tinggi total harus diisi.',
            'jarak_smb12.required'=> 'Jarak sumbu 1-2 harus diisi.',
            'rear_overhang.required'=> 'Rear overhang harus diisi.',
            'jarak_smb23.required'=>  'Jarak sumbu 2-3 harus diisi.',
            'front_overhang.required'=> 'Front overhang harus diisi.',
            'jarak_smb34.required'=>  'Jarak sumbu 3-4 harus diisi.',
            'img_tmpk_depan.required'=> 'Gambar tampak depan harus diisi.',//4mb
            'img_tmpk_smpng.required'=> 'Gambar tampak samping harus diisi.',
            'img_tmpk_blkng.required'=> 'Gambar tampak  belakang harus diisi.',
            'ukrn_smb1.required' => 'Ukuran sumbu 1 harus diisi.',
            'ukrn_smb2.required' => 'ukuran sumbu 2 harus diisi.',
            'ukrn_smb3.required' => 'Ukuran sumbu 3 harus diisi.',
            'ukrn_smb4.required' => 'Ukuran sumbu 4 harus diisi.',
            'kekuatan_rancangan_smb1' => 'Kekuatan ban pada sumbu 1',
            'kekuatan_rancangan_smb2' => 'Kekuatan ban pada sumbu 2',
            'kekuatan_rancangan_smb3' => 'Kekuatan ban pada sumbu 3',
            'kekuatan_rancangan_smb4' => 'Kekuatan ban pada sumbu 4',
            'tnggi_box_bak_tang.required' => 'Tinggi box/bak/tang harus diisi.',
            'rmh_bdn_2.required' => 'harus diisi.',
            'knfg_smb.required' => 'Konfigurasi sumbu harus diisi.',
            'kls_jln.required' => 'Kelas jalan harus diisi.',
            'lbr_pntu_spr.required' => 'Lebar pintu supir harus diisi.',
            'jrk_bngku.required' => 'Jarak bangku harus diisi.',
            'brt_ksng.required' => 'Berat kosong harus diisi.',
            'dya_angkt_brng.required' => 'Daya angkut barang harus diisi.',
            'dya_angkt_pnmpng.required' => 'Daya angkut penumpang harus diisi.',
            'kssuaian_lndsn.required' => 'Kesesuaian landasan harus diisi.',
            'kssuaian_bntk_fsk.required' => 'Kesesuaian bentuk fisik harus diisi.',
            'kssuaian_dmnsi_kndraan.required' => 'Kesesuaian dimensi kendaraan harus diisi.',
            'kssuaian_material.required' => 'Kesesuaian material harus diisi.',
            'kssuaian_pss_lmpu.required' => 'Kesesuaian posisi lampu harus diisi.',
            'kssuaian_ukrn.required' => 'Kesesuaian ukuran harus diisi.',
            'kssuaian_dmnsi_muatan' => 'Kesesuaian dimensi muatan harus diisi.',
            'kssuaian_vlm_muatan.required' => 'Kesesuaian volume muatan harus diisi.',
            'kssuaian_dmnsi_ddk.required' => 'Kesesuaian dimensi duduk harus diisi.',
            'kssuaian_jrk_ddk.required' => 'Kesesuaian jarak tempat duduk harus diisi.',
            'kssuaian_keluar_drrt.required' =>'Kesesuaian fasilitas tempat keluar darurat harus diisi.'

        ]);

    }

    public function index()
    {

        return view('blank');

    }

    public function home(){
        if(!Session::get('login')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        else{


        $datakendaraan = DB::table('datakendaraans')
        ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
        ->select('*')
        ->get();
        // $datakendaraan = Datakendaraan::all();

        return view('shares.index', compact('datakendaraan'));
        }
    }
    public function create()
    {
        if(!Session::get('login')){
            return redirect('login')->with('alert','Kamu harus login dulu');
        }
        else{

        return view('shares.create');
        }
    }

    public function store(Request $request)
    {
         $this->validateInsert($request);
         $destinationPath = '';
         $connection = DB::connection('mysql');
         $connection->beginTransaction();
         try {
                $savekendaraan = $connection->table('datakendaraans')
                                ->insert(
                                    [   'perusahaan_pembuat'=> $request->get('perusahaan_pembuat'),
                                        'alamat_pembuat'=> $request->get('alamat_pembuat'),
                                        'penanggungJwb_pembuat'=> $request->get('penanggungJwb_pembuat'),
                                        'no_srut'=> $request->get('no_srut'),
                                        'no_skrb'=> $request->get('no_skrb'),
                                        'tgl_skrb'=> $request->get('tgl_skrb'),
                                        'no_rangka'=> $request->get('no_rangka'),
                                        'no_mesin'=> $request->get('no_mesin'),
                                        'merek'=> $request->get('merek'),
                                        'jenis_kendaraan'=> $request->get('jenis_kendaraan'),
                                        'warna_kendaraan'=> $request->get('warna_kendaraan'),
                                        'lampiran'=> $request->get('lampiran'),
                                        'qrCode' => $destinationPath
                                    ]
                                );

                        //dd($savekendaraan);
            if($savekendaraan == true){
                $getID=$connection->table('datakendaraans')
                                ->where('no_srut', '=', $request->get('no_srut'))
                                ->first();

                // $no_srut = $getID->idkendaraan;
                $idkendaraan = $getID->idkendaraan;
                // $encrypted = Crypt::encryptString($idkendaraan);
                $destinationPath = $idkendaraan.'.png';
                $updatekendaraan = $connection->table('datakendaraans')
                ->where('idkendaraan','=', $idkendaraan)
                    ->update(
                        [
                            'qrCode'=> $destinationPath
                        ]
                );

                $qr = QrCode::size(500)
                ->format('png')
                ->generate('https://ujitipeditsrana.id/shares/datakendaraan/idkendaraan='.$idkendaraan, public_path('images/'.$idkendaraan.'.png'));

                // $qrCode = new QrCode('https://ujitipeditsrana.id/shares/datakendaraan/idkendaraan='.$idkendaraan);
                // $qrCode->setSize(300);
                // $qrCode->setMargin(10);
                // $qrCode->writeFile(public_path('images/'.$idkendaraan.'.png'));
                // ->generate('http://dishub.bekasi.com/id='.$idkendaraan, public_path('images/'.$request->get('no_srut').'.png'));
                // ->generate('http://dishub.bekasi.com/id='.$idkendaraan, public_path('images\\'.str_replace("/","-",$request->get('no_srut')).'.png'));


                $savedimensi = $connection
                            ->table('dimensis')
                            ->insert(
                                [
                                    'idkendaraan'=>  $idkendaraan,
                                    'panjang_ttl'=>  $request->get('panjang_ttl'),
                                    'lebar_ttl'=>  $request->get('lebar_ttl'),
                                    'tinggi_ttl'=>  $request->get('tinggi_ttl'),
                                    'jarak_smb12'=>  $request->get('jarak_smb12'),
                                    'rear_overhang'=>  $request->get('rear_overhang'),
                                    'jarak_smb23'=>  $request->get('jarak_smb23'),
                                    'front_overhang'=>  $request->get('front_overhang'),
                                    'jarak_smb34'=>  $request->get('jarak_smb34')
                                ]
                            );

                $savekekaroseri = $connection
                ->table('karoseris')
                ->insert(
                    [
                        'idkendaraan'=> $idkendaraan,
                        'jmlh_silinder'=> $request->get('jmlh_silinder'),
                        'isi_silinder'=> $request->get('isi_silinder'),
                        'pjg_boxbak'=> $request->get('pjg_boxbak'),
                        'tnggi_boxbaktang'=> $request->get('pjg_boxbak'),
                        'jmlh_tmpt_ddk'=> $request->get('pjg_boxbak'),
                        'daya_mtr'=> $request->get('daya_mtr'),
                        'lbr_boxbaktang'=> $request->get('lbr_boxbaktang'),
                        'rmh_bdn'=> $request->get('rmh_bdn'),
                        'jbb'=> $request->get('jbb'),
                        'jbkb'=> $request->get('jbkb'),
                        'jns_angkutan'=> $request->get('jns_angkutan'),
                        'jbi'=> $request->get('jbi'),
                        'jbki'=> $request->get('jbki'),
                        'bhan_bkr'=> $request->get('bhan_bkr'),
                        'sudut_pgi'=> $request->get('sudut_pgi'),
                        'pntu_darurat'=> $request->get('pntu_darurat'),
                        'varian'=> $request->get('varian'),
                        'kt_baktang'=> $request->get('kt_baktang')
                    ]
                );
                //gambar
                $imageName1 = 'IMGD'.$idkendaraan.'.'.request()->img_tmpk_depan->getClientOriginalExtension();
                request()->img_tmpk_depan->move(public_path('bentuk_fisik/images/'), $imageName1);
                $imageName2 = 'IMGS'.$idkendaraan.'.'.request()->img_tmpk_smpng->getClientOriginalExtension();
                request()->img_tmpk_smpng->move(public_path('bentuk_fisik/images/'), $imageName2);
                $imageName3 = 'IMGB'.$idkendaraan.'.'.request()->img_tmpk_blkng->getClientOriginalExtension();
                request()->img_tmpk_blkng->move(public_path('bentuk_fisik/images/'), $imageName3);

                $FileDestinationPath1 = 'bentuk_fisik/images/'. $imageName1;
                $FileDestinationPath2 = 'bentuk_fisik/images/'. $imageName2;
                $FileDestinationPath3 = 'bentuk_fisik/images/'. $imageName3;

                    $savebentukfisik = $connection
                    ->table('bentukfisiks')
                    ->insert(
                        [
                            'idkendaraan'=> $idkendaraan,
                            'img_tmpk_depan'=> $FileDestinationPath1,
                            'img_tmpk_smpng'=> $FileDestinationPath2,
                            'img_tmpk_blkng'=> $FileDestinationPath3
                        ]
                    );
                // endgambar
                    $saveketerangan= $connection
                    ->table('keterangans')
                    ->insert(
                        [
                            'idkendaraan'=> $idkendaraan,
                            'ukrn_smb1' => $request->get('ukrn_smb1'),
                            'ukrn_smb2' => $request->get('ukrn_smb2'),
                            'ukrn_smb3' => $request->get('ukrn_smb3'),
                            'ukrn_smb4' => $request->get('ukrn_smb4'),
                            'kekuatan_rancangan_smb1' => $request->get('kekuatan_rancangan_smb1'),
                            'kekuatan_rancangan_smb2' => $request->get('kekuatan_rancangan_smb2'),
                            'kekuatan_rancangan_smb3' => $request->get('kekuatan_rancangan_smb3'),
                            'kekuatan_rancangan_smb4' => $request->get('kekuatan_rancangan_smb4'),
                            'tnggi_box_bak_tang' => $request->get('tnggi_box_bak_tang'),
                            'rmh_bdn_2' => $request->get('rmh_bdn_2'),
                            'knfg_smb' => $request->get('knfg_smb'),
                            'kls_jln' => $request->get('kls_jln'),
                            'lbr_pntu_spr' => $request->get('lbr_pntu_spr'),
                            'jrk_bngku' => $request->get('jrk_bngku'),
                            'brt_ksng' => $request->get('brt_ksng'),
                            'dya_angkt_brng' => $request->get('dya_angkt_brng'),
                            'dya_angkt_pnmpng' => $request->get('dya_angkt_pnmpng')
                        ]
                    );

                    $saveceklis= $connection
                    ->table('cekliss')
                    ->insert(
                        [
                            'idkendaraan'=> $idkendaraan,
                            'kssuaian_lndsn' => $request->get('kssuaian_lndsn'),
                            'kssuaian_bntk_fsk' => $request->get('kssuaian_bntk_fsk'),
                            'kssuaian_dmnsi_kndraan' => $request->get('kssuaian_dmnsi_kndraan'),
                            'kssuaian_material' => $request->get('kssuaian_material'),
                            'kssuaian_pss_lmpu' => $request->get('kssuaian_pss_lmpu'),
                            'kssuaian_ukrn' => $request->get('kssuaian_ukrn'),
                            'kssuaian_dmnsi_muatan' => $request->get('kssuaian_dmnsi_muatan'),
                            'kssuaian_vlm_muatan' => $request->get('kssuaian_vlm_muatan'),
                            'kssuaian_dmnsi_ddk' => $request->get('kssuaian_dmnsi_ddk'),
                            'kssuaian_jrk_ddk' => $request->get('kssuaian_jrk_ddk'),
                            'kssuaian_keluar_drrt' => $request->get('kssuaian_keluar_drrt')
                        ]
                    );

            }

            $connection->commit();
            $result['code'] = 200;
            $result['status'] = 'Success';
            $result['message'] = 'Add Success';
        } catch (\Exception $e) {
            $connection->rollback();
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();

            $result['code'] = '-5001';
            $result['status'] = 'failed';
            $result['message'] = 'Internal Server Error';

            //bikin log
            Log::error($msg);
        } catch (\Illuminate\Database\QueryException $e) {
            $connection->rollback();
            $msg['file'] = $e->getFile();
            $msg['message'] = $e->getMessage();
            $msg['line'] = $e->getLine();

            $result['code'] = '-5002';
            $result['status'] = 'failed';
            $result['message'] = 'Database Error';

            //bikin log
            Log::error($msg);
        }

      return redirect('/home')->with($result['status'], $result['message']);
    }

    public function show()
    {
        $usersQuery = DB::table('datakendaraans')
        ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
        ->select('*')
        ->get();
        return datatables()->of($usersQuery)
            ->make(true);
    }

    public function edit($idkendaraan)
    {


        $datakendaraan = DB::table('datakendaraans')
        ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
        ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
        ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
        ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
        ->where('datakendaraans.idkendaraan','=', $idkendaraan)
        ->get();

        // $datakendaraan = datakendaraan::find($idkendaraan);

        return view('shares.edit', compact('datakendaraan'));
    }


    public function update(Request $request, $idkendaraan)
    {
    // dd(Input::hasFile('img_tmpk_depan'));
        $this->validateInsert($request);

        $FileDestinationPath1 = null;
        $FileDestinationPath2 = null;
        $FileDestinationPath3 = null;

        $imageName = DB::table('datakendaraans')
            ->join('bentukfisiks','bentukfisiks.idkendaraan','=','datakendaraans.idkendaraan')
            ->where('datakendaraans.idkendaraan','=', $idkendaraan)
            ->get();
            $image_pathDepan = public_path().'/'.$imageName[0]->img_tmpk_depan;  // Value is not URL but directory file path
            $image_pathSamping = public_path().'/'.$imageName[0]->img_tmpk_smpng;  // Value is not URL but directory file path
            $image_pathBelakang = public_path().'/'.$imageName[0]->img_tmpk_blkng;  // Value is not URL but directory file path


            $FileDestinationPath3= $request->img_tmpk_depan;
            //dd($request->img_tmpk_depan1);

        // dd(Input::hasFile('img_tmpk_smpng'));
        if((Input::hasFile('img_tmpk_depan') == true ) && (File::exists($image_pathDepan)))
        {
            File::delete($image_pathDepan);
            $imageName1 = 'IMGD'.$idkendaraan.'.'.$request->img_tmpk_depan->getClientOriginalExtension();
            $request->img_tmpk_depan->move(public_path('bentuk_fisik/images/'), $imageName1);
            $FileDestinationPath1 = 'bentuk_fisik/images/'. $imageName1;
        } else {
            $FileDestinationPath1= $request->img_tmpk_depan1;
        }

        if((Input::hasFile('img_tmpk_smpng') == true ) && (File::exists($image_pathSamping)))
        {
            File::delete($image_pathSamping);
            $imageName2 = 'IMGS'.$idkendaraan.'.'.$request->img_tmpk_smpng->getClientOriginalExtension();
            $request->img_tmpk_smpng->move(public_path('bentuk_fisik/images/'), $imageName2);
            $FileDestinationPath2 = 'bentuk_fisik/images/'. $imageName2;


        } else {
            $FileDestinationPath2= $request->img_tmpk_smpng2;
        }

        if((Input::hasFile('img_tmpk_blkng') == true ) && (File::exists($image_pathBelakang)))
        {
            File::delete($image_pathBelakang);
            $imageName3 = 'IMGB'.$idkendaraan.'.'.$request->img_tmpk_blkng->getClientOriginalExtension();
            $request->img_tmpk_blkng->move(public_path('bentuk_fisik/images/'), $imageName3);
            $FileDestinationPath3 = 'bentuk_fisik/images/'. $imageName3;

        } else {
            $FileDestinationPath3= $request->img_tmpk_blkng3;
        }


        $updatekendaraan = DB::table('datakendaraans')
                ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
                ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
                ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
                ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
                ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
                ->where('datakendaraans.idkendaraan','=', $idkendaraan)
                    ->update(
                        [
                            'perusahaan_pembuat'=> $request->nama_pembuat,
                            'alamat_pembuat'=> $request->alamat_pembuat,
                            'penanggungJwb_pembuat'=> $request->penanggungJwb_pembuat,
                            'no_srut'=> $request->no_srut,
                            'no_skrb'=> $request->no_skrb,
                            'tgl_skrb'=> $request->tgl_skrb,
                            'no_rangka'=> $request->no_rangka,
                            'no_mesin'=> $request->no_mesin,
                            'merek'=> $request->merek,
                            'jenis_kendaraan'=> $request->jenis_kendaraan,
                            'warna_kendaraan'=> $request->warna_kendaraan,
                            'lampiran'=> $request->lampiran,
                            'jmlh_silinder'=> $request->jmlh_silinder,
                            'isi_silinder'=> $request->isi_silinder,
                            'pjg_boxbak'=> $request->pjg_boxbak,
                            'tnggi_boxbaktang'=> $request->pjg_boxbak,
                            'jmlh_tmpt_ddk'=> $request->pjg_boxbak,
                            'daya_mtr'=> $request->daya_mtr,
                            'lbr_boxbaktang'=> $request->lbr_boxbaktang,
                            'rmh_bdn'=> $request->rmh_bdn,
                            'jbb'=> $request->jbb,
                            'jbkb'=> $request->jbkb,
                            'jns_angkutan'=> $request->jns_angkutan,
                            'jbi'=> $request->jbi,
                            'jbki'=> $request->jbki,
                            'bhan_bkr'=> $request->bhan_bkr,
                            'sudut_pgi'=> $request->sudut_pgi,
                            'pntu_darurat'=> $request->pntu_darurat,
                            'varian'=> $request->varian,
                            'kt_baktang'=> $request->kt_baktang,
                            'panjang_ttl'=>  $request->panjang_ttl,
                            'lebar_ttl'=>  $request->lebar_ttl,
                            'tinggi_ttl'=>  $request->tinggi_ttl,
                            'jarak_smb12'=>  $request->jarak_smb12,
                            'rear_overhang'=>  $request->rear_overhang,
                            'jarak_smb23'=>  $request->jarak_smb23,
                            'front_overhang'=>  $request->front_overhang,
                            'jarak_smb34'=>  $request->jarak_smb34,
                            'img_tmpk_depan'=> $FileDestinationPath1,
                            'img_tmpk_smpng'=> $FileDestinationPath2,
                            'img_tmpk_blkng'=> $FileDestinationPath3,
                            'ukrn_smb1' => $request->ukrn_smb1,
                            'ukrn_smb2' => $request->ukrn_smb2,
                            'ukrn_smb3' => $request->ukrn_smb3,
                            'ukrn_smb4' => $request->ukrn_smb4,
                            'kekuatan_rancangan_smb1' => $request->kekuatan_rancangan_smb1,
                            'kekuatan_rancangan_smb2' => $request->kekuatan_rancangan_smb2,
                            'kekuatan_rancangan_smb3' => $request->kekuatan_rancangan_smb3,
                            'kekuatan_rancangan_smb4' => $request->kekuatan_rancangan_smb4,
                            'tnggi_box_bak_tang' => $request->tnggi_box_bak_tang,
                            'rmh_bdn_2' => $request->rmh_bdn_2,
                            'knfg_smb' => $request->knfg_smb,
                            'kls_jln' => $request->kls_jln,
                            'lbr_pntu_spr' => $request->lbr_pntu_spr,
                            'jrk_bngku' => $request->jrk_bngku,
                            'brt_ksng' => $request->brt_ksng,
                            'dya_angkt_brng' => $request->dya_angkt_brng,
                            'dya_angkt_pnmpng' => $request->dya_angkt_pnmpng,
                            'kssuaian_lndsn' => $request->kssuaian_lndsn,
                            'kssuaian_bntk_fsk' => $request->kssuaian_bntk_fsk,
                            'kssuaian_dmnsi_kndraan' => $request->kssuaian_dmnsi_kndraan,
                            'kssuaian_material' => $request->kssuaian_material,
                            'kssuaian_pss_lmpu' => $request->kssuaian_pss_lmpu,
                            'kssuaian_ukrn' => $request->kssuaian_ukrn,
                            'kssuaian_dmnsi_muatan' => $request->kssuaian_dmnsi_muatan,
                            'kssuaian_vlm_muatan' => $request->kssuaian_vlm_muatan,
                            'kssuaian_dmnsi_ddk' => $request->kssuaian_dmnsi_ddk,
                            'kssuaian_jrk_ddk' => $request->kssuaian_jrk_ddk,
                            'kssuaian_keluar_drrt' => $request->kssuaian_keluar_drrt

                        ]
                    );


        return redirect('/home')->with(['success' => 'Data berhasil di update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($idkendaraan)
    {

        $imageName = DB::table('datakendaraans')
        ->join('bentukfisiks','bentukfisiks.idkendaraan','=','datakendaraans.idkendaraan')
        ->where('datakendaraans.idkendaraan','=', $idkendaraan)
        ->get();

        $image_pathQrCode = public_path().'/images/'.$imageName[0]->qrCode;
        $image_pathDepan = public_path().'/'.$imageName[0]->img_tmpk_depan;  // Value is not URL but directory file path
        $image_pathSamping = public_path().'/'.$imageName[0]->img_tmpk_smpng;  // Value is not URL but directory file path
        $image_pathBelakang = public_path().'/'.$imageName[0]->img_tmpk_blkng;  // Value is not URL but directory file path
        $image_pathBelakang = public_path().'/'.$imageName[0]->img_tmpk_blkng;
        if((File::exists($image_pathQrCode))||(File::exists($image_pathDepan)) || (File::exists($image_pathSamping)) || (File::exists($image_pathBelakang)) ) {
               File::delete($image_pathDepan, $image_pathSamping, $image_pathBelakang,$image_pathQrCode);
        }


        $deleteData= DB::connection('mysql')
            ->table('datakendaraans')
            ->join('dimensis', 'datakendaraans.idkendaraan', '=', 'dimensis.idkendaraan')
            ->join('karoseris', 'karoseris.idkendaraan', '=', 'dimensis.idkendaraan')
            ->join('bentukfisiks','bentukfisiks.idkendaraan','=','karoseris.idkendaraan')
            ->join('keterangans','keterangans.idkendaraan','=','bentukfisiks.idkendaraan')
            ->join('cekliss','cekliss.idkendaraan','=','keterangans.idkendaraan')
            ->where('datakendaraans.idkendaraan','=',$idkendaraan)
            ->where('dimensis.idkendaraan','=',$idkendaraan)
            ->where('karoseris.idkendaraan','=',$idkendaraan)
            ->where('bentukfisiks.idkendaraan','=',$idkendaraan)
            ->where('keterangans.idkendaraan','=',$idkendaraan)
            ->where('cekliss.idkendaraan','=',$idkendaraan)
            ->delete();

      return redirect('/home')->with('success', 'Stock has been deleted Successfully');
    }
}
