<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dimensi extends Model
{
    protected $fillable = [
        'panjang_ttl',
        'lebar_ttl',
        'tinggi_ttl',
        'jarak_smb12',
        'rear_overhang',
        'jarak_smb23',
        'front_overhang',
        'jarak_smb34'
  ];
}
