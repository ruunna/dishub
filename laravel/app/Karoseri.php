<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Karoseri extends Model
{
    protected $fillable = [
        'jmlh_silinder',
        'isi_silinder',
        'pjg_boxbak',
        'daya_mtr',
        'lbr_boxbaktang',
        'rmh_bdn',
        'jbb',
        'jbkb',
        'jns_angkutan',
        'jbi',
        'jbki',
        'bhan_bkr',
        'sudut_pgi',
        'pntu_darurat',
        'varian',
        'kt_baktang'
  ];
}
