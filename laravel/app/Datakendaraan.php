<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datakendaraan extends Model
{
    protected $fillable = [
        'idkendaraan',
        'perusahaan_pembuat',
        'alamat_pembuat',
        'penanggungJwb_pembuat',
        'no_srut',
        'no_skrb',
        'tgl_skrb',
        'no_rangka',
        'no_mesin',
        'merek',
        'jenis_kendaraan',
        'warna_kendaraan',
        'lampiran'
  ];
}
