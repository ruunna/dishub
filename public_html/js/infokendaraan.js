function getData(){
   $('#viewInfoList').DataTable({
      processing: true,
      serverSide: true,
      ajax: {
      url: "{{ url('/show') }}",
      type: 'GET'
      },
      columns: [
               { data: 'idkendaraan', name: 'idkendaraan' },
               { data: 'no_srut', name: 'no_srut' },
               { data: 'no_skrb', name: 'no_skrb' },
               { data: 'tgl_skrb', name: 'tgl_skrb' }
            ]
   
   });
}

